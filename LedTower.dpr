program LedTower;

uses
  Vcl.Forms,
  g_Form in 'g_Form.pas' {gForm},
  Vcl.Themes,
  Vcl.Styles,
  LedDisplay in 'LedDisplay.pas',
  uParserScript.IDE_Debuger in 'uParserScript.IDE_Debuger.pas',
  uLanguage in 'uLanguage.pas',
  uVisualSound in 'uVisualSound.pas',
  uSoundSpectrum in 'uSoundSpectrum.pas',
  uAnimateSound in 'uAnimateSound.pas',
  MMDeviceAPI in 'MMDeviceAPI.pas',
  uParseComCmd in 'uParseComCmd.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Onyx Blue');
  Application.CreateForm(TgForm, gForm);
  Application.Run;
end.
