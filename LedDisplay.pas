unit LedDisplay;

interface

uses SysUtils, Classes, Controls, Graphics, Math, Windows, Dialogs, ExtCtrls, Messages, System.Generics.Collections,
     System.Generics.Defaults;


type
  TLeds = record
  public
    Vis : Boolean;
    Color : TColor;
  end;

  TLedRectFrame = class
  private
    FPosPt : TList<TRect>;
    FPosPtPie : TList<TRect>;
    FSizeRect : TRect;
    FPosNumber : TList<TPoint>;
    constructor Create;
    destructor Destroy; override;
    property PosPt : TList<TRect> read FPosPt write FPosPt;
  end;

  TLedDisplay = class(TCustomControl)
  private
    FRows, FCols : Word;
    FDotRadius : Word;
    FIntendetionAtBorder : Word;
    FIntendetionAtFrame : TPoint;
    FStepFrame : Integer;
    FLeftNumber : Boolean;
    FLedRectList : TObjectList<TLedRectFrame>;
    FFontNumber : TFont;
    FPosNumberLeft : TList<TPoint>;
    FMouseDown : Boolean;
    FMouseButton: TMouseButton;
    FTimerPaintLed : TTimer;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure EllipseDotLedCalc;
    procedure Paint; override;
    procedure SetFontNumber(const Value: TFont);
    procedure SetRows(const Value: Word);
    procedure SetCols(const Value: Word);
    procedure OnTimer(Sender: TObject);
    procedure SetRowCol;
    function GetFontNumberColor: TColor;
    procedure SetFontNumberColor(const Value: TColor);
  protected
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;  X, Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;  X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    property LedRectList : TObjectList<TLedRectFrame> read FLedRectList write FLedRectList;
    procedure ReSize; override;
  public
    ColorLedPenActive : TColor;  //������ ���������� ���������
    ColorLedBrushActive : TColor; //������� ���������� ���������
    ColorLedPenOff : TColor;  //������ ����������
    ColorLedBrushOff : TColor; //������� ����������
    ColorPie : TColor;
    Leds : array of array of TLeds;
    DefRows, DefCols : Word;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Calc;
    procedure PaintOnlyLed;
    procedure LED_off;
    procedure LED_on;
  published
    property FontNumber: TFont read FFontNumber write SetFontNumber;
    property Rows : Word read FRows write SetRows;
    property Cols : Word read FCols write SetCols;
    property FontNumberColor : TColor read GetFontNumberColor write SetFontNumberColor;
  end;

implementation

{===========================================TLedDisplay================================================================}

procedure TLedDisplay.Calc;
var
  lrf : TLedRectFrame;
  pt : TPoint;
  x1, y1, x2,
  step_y, //������ ������
  y_text, x_text  : integer;
  i, c : SmallInt;
begin
  if FLeftNumber
  then begin
    Canvas.Font.Assign(FFontNumber);
//    c_text := Canvas.TextHeight('88') shl 1;
    x1 := Canvas.TextWidth('88') + FIntendetionAtBorder shl 1;
    x_text := FIntendetionAtBorder;
    FPosNumberLeft.Clear;
  end
  else
  x1 := FIntendetionAtBorder;
  y1 := FIntendetionAtBorder;

  x2 := Width - FIntendetionAtBorder - FDotRadius shr 1;

  step_y := (Height - FIntendetionAtBorder shl 1 - FStepFrame * (FRows-1)) div FRows;
  FPosNumberLeft.Clear;

//���������� �����
  for lrf in FLedRectList do
  begin
    lrf.FSizeRect.TopLeft.X := x1;
    lrf.FSizeRect.TopLeft.Y := y1;

    Inc(y1, step_y + FStepFrame);

    lrf.FSizeRect.BottomRight.X := x2;
    lrf.FSizeRect.BottomRight.Y := lrf.FSizeRect.TopLeft.Y + step_y;

    if FLeftNumber then
    begin
      Canvas.Font.Assign(FFontNumber);
      y_text := (step_y shr 1) div (Canvas.TextHeight('888') shr 1) + lrf.FSizeRect.TopLeft.Y + FIntendetionAtBorder;
      x_text := x1 shr 1 - Canvas.TextWidth(FLedRectList.Count.ToString) shr 1;
      FPosNumberLeft.Add(Point(x_text, y_text));
    end;
  end;

  EllipseDotLedCalc;
end;

constructor TLedDisplay.Create(AOwner: TComponent);
var
  i, j : SmallInt;
begin
  inherited;
  Width   := 150;   Height  := 500;
  FRows   := 16;    FCols   := 16;
  DefRows := 16;    DefCols := 16;

  FIntendetionAtBorder := 5;
  FIntendetionAtFrame.X := 0;  FIntendetionAtFrame.Y := 10;

  FDotRadius := 3;
  FStepFrame := 10;

  FLeftNumber := true;
  FFontNumber := TFont.Create;
  FFontNumber.Color := clWhite;
  FFontNumber.Size := 8;
  FFontNumber.Style := [];

  FPosNumberLeft := TList<TPoint>.Create;

  FLedRectList := TObjectList<TLedRectFrame>.Create;

  ColorLedPenActive := clRed;
  ColorLedBrushActive := clYellow;
  ColorLedPenOff := RGB(75, 75, 0);
  ColorLedBrushOff := RGB(75, 75, 0);
  ColorPie := RGB(23, 23, 23);

  FMouseDown := false;

  //  ������� ��������� �����������
  SetLength(Leds, FRows);
  for i := 0 to FRows-1 do SetLength(Leds[i], FCols);

  for i := 0 to FRows-1 do
    for j := 0 to FCols-1 do
    begin
      Leds[i, j].Vis := false;
      Leds[i, j].Color := clYellow;
    end;

  FLedRectList.Clear;
  for i := 0 to FRows-1 do
    FLedRectList.Add(TLedRectFrame.Create);

//  SetRowCol;

  FTimerPaintLed := TTimer.Create(Self);
  FTimerPaintLed.Interval := 20;
  FTimerPaintLed.OnTimer := OnTimer;
//  FTimerPaintLed.Enabled := true;
end;

procedure TLedDisplay.EllipseDotLedCalc;
const
  dToRad = PI / 180.0;
var
  rc: TRect;
//  const iParts : integer
  x, y : Integer;
  x2, y2 : Integer;
  x0, y0 : Double;
  a, b, anglePart : Double;
  i, i2 : Integer;
  LRL : TLedRectFrame;
  AngleStart : integer;
begin
  for LRL in FLedRectList do
  begin
    rc := LRL.FSizeRect;

    x0 := (rc.left + rc.right ) / 2;
    y0 := (rc.top + rc.bottom)  / 2;
    a  := (rc.right - rc.left ) / 2;
    b  := (rc.bottom - rc.top ) / 2;
    anglePart := 360.0 / FCols;

    LRL.FPosPt.Clear;     //������� �����������
    LRL.FPosPtPie.Clear;  //����� ��� ��������� ������� �� ������

    AngleStart := -180;  //�� 9 ����� ������ ���������
    for i := 0 to FCols-1 do
    begin
      x := Round(x0 + a * cos( ( i * anglePart + AngleStart ) * dToRad ));
      y := Round(y0 + b * sin( ( i * anglePart + AngleStart ) * dToRad ));
//������� �����������
      LRL.FPosPt.Add(Rect(x - FDotRadius, y - FDotRadius, x + FDotRadius, y + FDotRadius));

//������� �������
      x2 := Round(x0 + a * cos( ( i * anglePart + anglePart + AngleStart  ) * dToRad ));
      y2 := Round(y0 + b * sin( ( i * anglePart + anglePart + AngleStart  ) * dToRad ));
      LRL.FPosPtPie.Add(Rect(x2, y2, x, y));
    end;
  end; // for LRL in FLedRectList do

end;

function TLedDisplay.GetFontNumberColor: TColor;
begin
  Result := FFontNumber.Color;
end;

procedure TLedDisplay.LED_off;
var
  i, j : word;
begin
  inherited;

  for i := 0 to FRows-1 do
    for j := 0 to FCols-1 do
      Leds[i, j].Vis := false;
end;

procedure TLedDisplay.LED_on;
var
  i, j : word;
begin
  inherited;

  for i := 0 to FRows-1 do
    for j := 0 to FCols-1 do
      Leds[i, j].Vis := true;
end;

procedure TLedDisplay.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FMouseDown := True;
  FMouseButton := Button;
end;

procedure TLedDisplay.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  find : Boolean;
  i, j : word;
  ptr : TRect;
begin
  inherited;

  if FMouseDown then
  for i := 0 to FRows-1 do
  begin
    for j := 0 to FCols-1 do
    begin
      ptr := FLedRectList[i].FPosPt[j];
      if ptr.Contains(Point(x, y)) then
      begin
        find := true;
        if mbLeft = FMouseButton then Leds[i, j].Vis := true else Leds[i, j].Vis := false;
        break
      end;

    end;
    if find then
    begin
      PaintOnlyLed;
      break;
    end;
  end;

end;

procedure TLedDisplay.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  find : Boolean;
  i, j : word;
  ptr : TRect;
begin
  inherited MouseUp(Button, Shift, X, Y);
  FMouseDown := false;

  find := false;
  for i := 0 to FRows-1 do
  begin
    for j := 0 to FCols-1 do
    begin
      ptr := FLedRectList[i].FPosPt[j];
      if ptr.Contains(Point(x, y)) then
      begin
        find := true;
        Leds[i, j].Vis := not Leds[i, j].Vis;
        break
      end;
    end;
    if find then
    begin
      PaintOnlyLed;
      break;
    end;
  end;
end;

procedure TLedDisplay.OnTimer(Sender: TObject);
begin
//  PaintOnlyLed;
end;

procedure TLedDisplay.Paint;
var
  pt : TPoint;
  ptr : TRect;
  LRL : TLedRectFrame;
  i : word;
begin
  inherited;
//paint background
  Canvas.Pen.Color := clBlack;
  Canvas.Brush.Color := clBlack;
  Canvas.Rectangle(0, 0, Width, Height);

//paint  ellipse
  canvas.Pen.Color := ColorPie;
  canvas.Brush.Color := clNone;

////paint ellipse pie
  for LRL in FLedRectList do
    for ptr in LRL.FPosPtPie do
      canvas.Pie(LRL.FSizeRect.Left, LRL.FSizeRect.Top, LRL.FSizeRect.Right, LRL.FSizeRect.Bottom,
                ptr.TopLeft.X, ptr.TopLeft.Y, ptr.BottomRight.X, ptr.BottomRight.Y);


  if FLeftNumber then
  begin
    Canvas.Font.Assign(FFontNumber);
//paint lvl number
    i := 1;
    for pt in FPosNumberLeft do
    begin
      Canvas.TextOut(pt.X, pt.Y, i.ToString);
      Inc(i);
    end;
  end;

//paint LED's
  PaintOnlyLed;;
end;

procedure TLedDisplay.PaintOnlyLed;
var
  i, j : word;
  ptr : TRect;
begin
  for i := 0 to FRows-1 do
    for j := 0 to FCols-1 do
    begin
      if Leds[i, j].Vis then
      begin
        canvas.Pen.Color   := ColorLedPenActive;
        canvas.Brush.Color := ColorLedBrushActive;
      end
      else
      begin
        canvas.Pen.Color   := ColorLedPenOff;
        canvas.Brush.Color := ColorLedBrushOff;
      end;

      ptr := FLedRectList[i].FPosPt[j];
      canvas.Ellipse(ptr.TopLeft.X, ptr.TopLeft.Y, ptr.BottomRight.X, ptr.BottomRight.Y);
    end;
end;

procedure TLedDisplay.ReSize;
begin
  inherited;
  Calc;
end;

procedure TLedDisplay.SetCols(const Value: Word);
begin
  if Value < 1 then exit;
  FCols := Value;
  SetRowCol;
end;

procedure TLedDisplay.SetFontNumber(const Value: TFont);
begin
  FFontNumber.Assign(Value);
end;

procedure TLedDisplay.SetFontNumberColor(const Value: TColor);
begin
  if Value <> FFontNumber.Color then
  begin
    FFontNumber.Color := Value;
  end;
end;

procedure TLedDisplay.SetRowCol;
var
  i, j, c : SmallInt;
begin
  Leds := nil;

//  ������� ��������� �����������
  SetLength(Leds, FRows);
  for i := 0 to FRows-1 do SetLength(Leds[i], FCols);

  for i := 0 to Rows-1 do
    for j := 0 to Cols-1 do
    begin
      Leds[i, j].Vis := false;
      Leds[i, j].Color := clYellow;
    end;

  FLedRectList.Clear;
  c := FRows-1;
  for i := 0 to c do
    FLedRectList.Add(TLedRectFrame.Create);

  Calc;
  Paint;
end;

procedure TLedDisplay.SetRows(const Value: Word);
begin
  if (Value < 1)or(Value=FRows) then exit;
  FRows := Value;
  SetRowCol;
end;

destructor TLedDisplay.Destroy;
var
  i, c : ShortInt;
begin
  FreeAndNil(FTimerPaintLed);
  Leds := nil;

  FLedRectList.Clear;
  FreeAndNil(FLedRectList);
  FreeAndNil(FFontNumber);

  FPosNumberLeft.Clear;
  FreeAndNil(FPosNumberLeft);
  inherited;
end;

procedure TLedDisplay.WMPaint(var Message: TWMPaint);
begin
  inherited;
end;

{========================================= TLedRectFrame ==============================================================}

constructor TLedRectFrame.Create;
begin
  inherited;
  FPosPt := TList<TRect>.Create;
  FPosNumber := TList<TPoint>.Create;
  FPosPtPie := TList<TRect>.Create;
end;

destructor TLedRectFrame.Destroy;
begin
  FPosPt.Clear;
  FreeAndNil(FPosPt);

  FPosNumber.Clear;
  FreeAndNil(FPosNumber);

  FPosPtPie.Clear;
  FreeAndNil(FPosPtPie);
  inherited;
end;

end.
