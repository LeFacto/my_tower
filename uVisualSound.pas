unit uVisualSound;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs;

type
  TVisualSoundLevel = class(TCustomControl)
  private
    FPos, FPosMax, FPosMin : Integer;
    FBorder : Word;
    FCountRectangle : Word;
    FGreenZonePercent, FYellowZonePercent, FRedZonePercent : Byte;
    FGreenZoneHeight, FYellowZoneHeight, FRedZoneHeight : Integer;
    gh, yh, rh : Integer;
    FStep : Integer;
    FReal_pos : Integer;
    FWidthColumn : Word;
    procedure SetPos(const Value: Integer);
    procedure CalcPoint;
    procedure SetMax(const Value: Integer);
    procedure SetMin(const Value: Integer);
  protected
    procedure Paint; override;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Position : Integer read FPos write SetPos;
    property Max:Integer read FPosMax write SetMax;
    property Min:Integer read FPosMin write SetMin;
  end;

  TGradientDirection = (gdHorizontal, gdVertical);
  TPointArr = array of TPoint;
  PPointArr = ^TPointArr;

  TVisualSoundLevelTime = class(TCustomControl)
  private
    bmpBack, bmpMask : TBitmap;
    fCountPoint : Integer;
    procedure PrepareToPaint;
    procedure GradientFillCanvas(Canvas: TCanvas; StartColor, EndColor: TColor; const ARect: TRect; Direction: TGradientDirection);
    procedure SetCountPoint(const Value: Integer);
  protected
    procedure Paint; override;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
  public
    Points : TPointArr;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
  published
    property CountPoint : Integer read fCountPoint write SetCountPoint;
  end;

//  TColumnSoundSpectr = record
//  private
//    x, y : Integer;
//  public
//    amp : Integer;
//  end;
//  TColumnSoundSpectrArr = array of TColumnSoundSpectr;

  vssMode = (vssmLine, vssmUnionColumn, vssmColumn);

  TVisualSoundSpectr = class(TCustomControl)
  private
    bmpBack, bmpMask : TBitmap;
    fMaxSpectr : Integer;
    fCountColumn : Integer;
    fWidthColumn : Word;
    xScale, yScale : Real;
    fFixedHeight : Boolean;
    fMode : vssMode;
    fMaxHeightColumn : Real;  //������������� ������������ �������� ��� �������
    fColumnIndention : Word;
    procedure SetCountColumn(const Value: Integer);
    procedure PrepareToPaint;
    procedure GradientFillCanvas(Canvas: TCanvas; StartColor, EndColor: TColor; const ARect: TRect; Direction: TGradientDirection);
    procedure SetMaxHeightColumn(const Value: Real);
    procedure setMode(const Value: vssMode);
    procedure SetFixedHeight(const Value: Boolean);
  protected
    procedure Paint; override;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
  public
    ColumnAmpl : array of Double;
    ColumnPoint : TPointArr;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure CalcAndPaint;
  published
    property CountColumn : Integer read fCountColumn write SetCountColumn;
    property MaxHeightColumn : Real read fMaxHeightColumn write SetMaxHeightColumn;
    property Mode : vssMode read fMode write setMode;
    property FixedHeight : Boolean read fFixedHeight write SetFixedHeight;
  end;

  function ConToReal(value, From1, From2, To1, To2 :Integer):Integer;

implementation

function ConToReal(value, From1, From2, To1, To2 :Integer):Integer;
begin
  result := Trunc((value-From1)/(From2-From1)*(To2-To1)+To1);
end;

{ TVisualSoundBar }

procedure TVisualSoundLevel.CalcPoint;
var
  h : integer;
begin
  FCountRectangle := (Self.Height-(FBorder*2)) div FStep;
  h := Height - FBorder * 2;
  FGreenZoneHeight  := h - (FGreenZonePercent * h) div 100;
  FYellowZoneHeight := FGreenZoneHeight - (FYellowZonePercent * h) div 100;
  FRedZoneHeight    := FYellowZoneHeight - (FRedZonePercent * h) div 100;

  FReal_pos := Height - FBorder*2 - ConToReal(FPos, 0, FPosMax-FPosMin, 0, Self.Height-FBorder*2);

  yh :=-1; rh := -1;
  if (FReal_pos <= Height-FBorder)and(FReal_pos > FGreenZoneHeight) then
  begin
    gh := FReal_pos;
    exit;
  end
  else gh := FGreenZoneHeight;

  if (FReal_pos <= FGreenZoneHeight)and(FReal_pos > FYellowZoneHeight) then
  begin
    yh := FReal_pos;
    exit;
  end
  else yh := FYellowZoneHeight;

  if (FReal_pos <= FYellowZoneHeight)and(FReal_pos > FRedZoneHeight) then rh := FReal_pos else  rh := FRedZoneHeight;
end;

constructor TVisualSoundLevel.Create(AOwner: TComponent);
begin
  inherited;
  Width := 15;
  Height := 102;
  FPosMax := 100;
  FPosMin := 0;
  FPos := 0;
  FBorder := 1;

  FStep := 5; //��� ������ �������������� � ��������

  //������������ ������ ��������� � ���������
  FGreenZonePercent := 70;
  FYellowZonePercent := 20;
  FRedZonePercent := 10;
  CalcPoint;
end;

destructor TVisualSoundLevel.Destroy;
begin

  inherited;
end;

procedure TVisualSoundLevel.Paint;
var
  i, k : integer;
begin
  inherited;
  Canvas.Pen.Color := clBlack;
  Canvas.Brush.Color := clBlack;
  Canvas.Rectangle(0, 0, Width, Height);

//������ ����� ���������� �������
  Canvas.Brush.Color := RGB(127, 0, 0);
  Canvas.Pen.Color := RGB(127, 0, 0);
  Canvas.Rectangle(FBorder, FBorder, Width-FBorder, FYellowZoneHeight+FBorder);

  Canvas.Brush.Color := RGB(127, 127, 0);
  Canvas.Pen.Color := RGB(127, 127, 0);
  Canvas.Rectangle(FBorder, FYellowZoneHeight+FBorder, Width-FBorder, FGreenZoneHeight+FBorder);

  Canvas.Brush.Color := RGB(0, 127, 0);
  Canvas.Pen.Color := RGB(0, 127, 0);
  Canvas.Rectangle(FBorder, FGreenZoneHeight+FBorder, Width-FBorder, Height-FBorder);

//������ ����� ���������� ��������
  Canvas.Brush.Color := clLime;
  Canvas.Pen.Color := clLime;
  Canvas.Rectangle(FBorder, gh+FBorder, Width-FBorder, Height-FBorder);

  if yh <> -1 then
  begin
    Canvas.Brush.Color := clYellow;
    Canvas.Pen.Color := clYellow;
    Canvas.Rectangle(FBorder, yh+FBorder, Width-FBorder, gh+FBorder);
  end;

  if rh <> -1 then
  begin
    Canvas.Brush.Color := clRed;
    Canvas.Pen.Color := clRed;
    Canvas.Rectangle(FBorder, rh+FBorder, Width-FBorder, yh+FBorder);
  end;

//������ �������������� �����
  Canvas.Pen.Color := clBlack;
  Canvas.MoveTo(0, FStep+FBorder);
  for i := 1 to FCountRectangle do
  begin
    Canvas.LineTo(Self.Width, i*FStep+FBorder);
    Canvas.MoveTo(0, (i+1)*FStep+FBorder);
  end;
end;

procedure TVisualSoundLevel.SetMax(const Value: Integer);
begin
  if Value <= FPosMin then FPosMax := FPosMin + 1 else FPosMax := Value;
end;

procedure TVisualSoundLevel.SetMin(const Value: Integer);
begin
  if Value >= FPosMax then FPosMin := FPosMax - 1 else FPosMin := Value;
end;

procedure TVisualSoundLevel.SetPos(const Value: Integer);
begin
  if (Value > FPosMax)or(Value < FPosMin) then FPos := FPosMin else FPos := Value;
  CalcPoint;
  Repaint;
end;

procedure TVisualSoundLevel.WMSize(var Message: TWMSize);
begin
  CalcPoint;
end;

{ TVisualSoundLevelTime ========================================================================}

procedure TVisualSoundLevelTime.Clear;
begin
//������� ����
  Canvas.Brush.Color := clBlack;
  Canvas.FillRect(Rect(0, 0, Width, Height));
//����������� ����� �����
  Canvas.Pen.Color := RGB(70, 70, 70);
  Canvas.Pen.Style := psDot;
  Canvas.MoveTo(0, Height shr 1);
  Canvas.LineTo(Width, Height shr 1);
end;

constructor TVisualSoundLevelTime.Create(AOwner: TComponent);
begin
  inherited;
  Width := 600;
  Height := 200;

  fCountPoint := 0;
  SetLength(Points, 0);
//  Points[0].X := 000; Points[0].Y := 100;
//  Points[1].X := 100; Points[1].Y := 40;
//  Points[2].X := 200; Points[2].Y := 200;
//  Points[3].X := 300; Points[4].Y := 68;
//  Points[4].X := 400; Points[5].Y := 122;
//  Points[5].X := 500; Points[5].Y := 180;
//  Points[6].X := 600; Points[6].Y := 20;
//  Points[7].X := 700; Points[7].Y := 100;

  bmpBack := TBitmap.Create;
  bmpBack.PixelFormat := pf24bit;

  bmpMask := TBitmap.Create;
  bmpMask.PixelFormat := pf1bit;

  PrepareToPaint;
end;

destructor TVisualSoundLevelTime.Destroy;
begin
  FreeAndNil(bmpBack);
  FreeAndNil(bmpMask);
  Points := nil;
  inherited;
end;

procedure TVisualSoundLevelTime.GradientFillCanvas(Canvas: TCanvas; StartColor, EndColor: TColor; const ARect: TRect; Direction: TGradientDirection);
const
  cGradientDirections: array[TGradientDirection] of Cardinal =
  (GRADIENT_FILL_RECT_H, GRADIENT_FILL_RECT_V);
type
  TTriVertex = packed record
    X,Y :LongInt;
    Red,Green,Blue,Alpha: Word;
  end;
var
  Vertexes: array[0..1] of TTriVertex;
  GradientRect: TGradientRect;
begin
  StartColor:=ColorToRGB(StartColor);
  EndColor:=ColorToRGB(EndColor);

  Vertexes[0].X:=ARect.Left;
  Vertexes[0].Y:=ARect.Top;
  Vertexes[0].Red:=GetRValue(StartColor) shl 8;
  Vertexes[0].Blue:=GetBValue(StartColor) shl 8;
  Vertexes[0].Green:=GetGValue(StartColor) shl 8;
  Vertexes[0].Alpha:=0;

  Vertexes[1].X:=ARect.Right;
  Vertexes[1].Y:=ARect.Bottom;
  Vertexes[1].Red:=GetRValue(EndColor) shl 8;
  Vertexes[1].Blue:=GetBValue(EndColor) shl 8;
  Vertexes[1].Green:=GetGValue(EndColor) shl 8;
  Vertexes[1].Alpha:=0;

  GradientRect.UpperLeft:=0;
  GradientRect.LowerRight:=1;

  GradientFill(Canvas.Handle, PTriVertex(@Vertexes[0]), 2, @GradientRect, 1, cGradientDirections[Direction]);
end;

procedure TVisualSoundLevelTime.Paint;
begin
  inherited;
//������� ����
  Canvas.Brush.Color := clBlack;
  Canvas.FillRect(Rect(0, 0, Width, Height));
//����������� ����� �����
  Canvas.Pen.Color := RGB(70, 70, 70);
  Canvas.Pen.Style := psDot;
  Canvas.MoveTo(0, Height shr 1);
  Canvas.LineTo(Width, Height shr 1);


  with bmpMask.Canvas do
  begin
//    Pen.Color := clWhite;
    Brush.Color := clWhite;
    FillRect(Rect(0, 0, bmpMask.Width, bmpMask.Height));

    Pen.Color := clBlack;
//    Pen.Width := 1;
    Polyline(Points);
  end;

  MaskBlt(Canvas.Handle, 0, 0, bmpBack.Width, bmpBack.Height, bmpBack.Canvas.Handle, 0, 0, bmpMask.Handle, 0, 0, MakeROP4(PATCOPY xor PATINVERT, SRCCOPY));
end;

procedure TVisualSoundLevelTime.PrepareToPaint;
var
  greenH, yelloyH, redH, h : Integer;
begin
  bmpMask.SetSize(Width, Height);
  bmpBack.SetSize(Width, Height);

  greenH := Round((Height shr 1) * 0.35);
  yelloyH := Round((Height shr 1) * 0.3);
  redH := Round((Height shr 1) * 0.35);

//������ �������� ���������
  h := 0;
  GradientFillCanvas(bmpBack.Canvas, clRed, clYellow, rect(0, h, bmpBack.Width, redH), gdVertical);
  Inc(h, redH);
  GradientFillCanvas(bmpBack.Canvas, clYellow, clGreen, rect(0, h, bmpBack.Width, h+yelloyH), gdVertical);
  Inc(h, yelloyH);
  GradientFillCanvas(bmpBack.Canvas, clGreen, clGreen, rect(0, h, bmpBack.Width, h+greenH), gdVertical);
  Inc(h, greenH);
//������ ���������� �����
  GradientFillCanvas(bmpBack.Canvas, clGreen, clGreen, rect(0, h, bmpBack.Width, h+greenH), gdVertical);
  Inc(h, greenH);
  GradientFillCanvas(bmpBack.Canvas, clGreen, clYellow, rect(0, h, bmpBack.Width, h+yelloyH), gdVertical);
  Inc(h, yelloyH);
  GradientFillCanvas(bmpBack.Canvas, clYellow, clRed, rect(0, h, bmpBack.Width, h+redH), gdVertical);

//  GradientFillCanvas(bmpBack.Canvas, clRed, clGreen, rect(0, 0, bmpBack.Width, bmpBack.Height), gdVertical);
end;

procedure TVisualSoundLevelTime.SetCountPoint(const Value: Integer);
begin
  if (Value > 0)or(Value <> fCountPoint) then
  begin
    fCountPoint := Value;
    SetLength(Points, Value);
  end;
end;

procedure TVisualSoundLevelTime.WMSize(var Message: TWMSize);
begin
  PrepareToPaint;
end;

{ TVisualSoundSpectr ==========================================================}

procedure TVisualSoundSpectr.CalcAndPaint;
var
  ampl : Integer;
  tmpWidthColumn : Real;
  i, c : Word;
begin
  if fCountColumn = 0 then exit;

  if fFixedHeight then
  begin
    yScale := Height / fMaxHeightColumn;
    case fMode of
      vssmLine :        xScale := Width / (fCountColumn - 1);
      vssmUnionColumn : xScale := Width / fCountColumn;
      vssmColumn      : xScale := ((Width-fColumnIndention) / fCountColumn) {- (fCountColumn / 4)};
    end;


    if fMode = vssmLine then
    begin
      c := fCountColumn-1;
      for i := 0 to c do
      begin
        ColumnPoint[i].X := Trunc(xScale * i);
        ColumnPoint[i].Y := Height - Trunc(yScale * ColumnAmpl[i]);
      end;
    end;


    if fMode = vssmUnionColumn then
    begin
      fWidthColumn := Trunc(Width / fCountColumn);
      c := fCountColumn-1;
      for i := 0 to c do
      begin
        ColumnPoint[i].X := Trunc(xScale * i);
        ColumnPoint[i].Y := Height - Trunc(yScale * ColumnAmpl[i]);
      end;
    end;

    if fMode = vssmColumn then
    begin
      fWidthColumn := Round(xScale);
      c := fCountColumn-1;
      ColumnPoint[0].X := 4;
      ColumnPoint[0].Y := Height - Trunc(yScale * ColumnAmpl[0]);
      for i := 0 to c do
      begin
        ColumnPoint[i].X := (fWidthColumn * i) + fColumnIndention;
        ColumnPoint[i].Y := Height - Trunc(yScale * ColumnAmpl[i]);
      end;
    end;
  end;


//����� ������������� �������� ������
//  fMaxSpectr := 0;
//  for ampl in ColumnAmpl do
//    if fMaxSpectr < ampl then fMaxSpectr := ampl;

//  tmpWidthColumn := Width / fCountColumn;

  Paint;
end;

constructor TVisualSoundSpectr.Create(AOwner: TComponent);
begin
  inherited;
  Width := 600;
  Height := 140;

  fCountColumn := 0;
  fMaxSpectr := 0;
  fFixedHeight := false;
  fMode := vssmLine;
  fMaxHeightColumn := 100; //------------------------------------------
  fColumnIndention := 4;

  bmpBack := TBitmap.Create;
  bmpBack.PixelFormat := pf24bit;

  bmpMask := TBitmap.Create;
  bmpMask.PixelFormat := pf1bit;

  PrepareToPaint;
end;

destructor TVisualSoundSpectr.Destroy;
begin
  ColumnAmpl := nil;
  ColumnPoint := nil;
  FreeAndNil(bmpBack);
  FreeAndNil(bmpMask);
  inherited;
end;

procedure TVisualSoundSpectr.GradientFillCanvas(Canvas: TCanvas; StartColor, EndColor: TColor; const ARect: TRect; Direction: TGradientDirection);
const
  cGradientDirections: array[TGradientDirection] of Cardinal = (GRADIENT_FILL_RECT_H, GRADIENT_FILL_RECT_V);
type
  TTriVertex = packed record
    X,Y :LongInt;
    Red,Green,Blue,Alpha: Word;
  end;
var
  Vertexes: array[0..1] of TTriVertex;
  GradientRect: TGradientRect;
begin
  StartColor:=ColorToRGB(StartColor);
  EndColor:=ColorToRGB(EndColor);

  Vertexes[0].X:=ARect.Left;
  Vertexes[0].Y:=ARect.Top;
  Vertexes[0].Red:=GetRValue(StartColor) shl 8;
  Vertexes[0].Blue:=GetBValue(StartColor) shl 8;
  Vertexes[0].Green:=GetGValue(StartColor) shl 8;
  Vertexes[0].Alpha:=0;

  Vertexes[1].X:=ARect.Right;
  Vertexes[1].Y:=ARect.Bottom;
  Vertexes[1].Red:=GetRValue(EndColor) shl 8;
  Vertexes[1].Blue:=GetBValue(EndColor) shl 8;
  Vertexes[1].Green:=GetGValue(EndColor) shl 8;
  Vertexes[1].Alpha:=0;

  GradientRect.UpperLeft:=0;
  GradientRect.LowerRight:=1;

  GradientFill(Canvas.Handle, PTriVertex(@Vertexes[0]), 2, @GradientRect, 1, cGradientDirections[Direction]);
end;

procedure TVisualSoundSpectr.Paint;
var
  i, c : Integer;
begin
  inherited;
  Canvas.Brush.Color := clBlack;
  Canvas.FillRect(Rect(0, 0, Width, Height));

  if CountColumn = 0 then exit;

  c := CountColumn - 1;
  if fMode = vssmLine then
  begin
    Canvas.Pen.Color := clWhite;
    Canvas.Polyline(ColumnPoint);
  end
  else
  if (fMode = vssmUnionColumn) then
  begin
    bmpMask.Canvas.Brush.Color := clWhite;
    bmpMask.Canvas.FillRect(Rect(0, 0, bmpMask.Width, bmpMask.Height));

    bmpMask.Canvas.Brush.Color := clBlack;
    c := fCountColumn-2;
    for i := 0 to c do
//      bmpMask.Canvas.FillRect(Rect(ColumnPoint[i].X, ColumnPoint[i].Y, fWidthColumn, Height));
    bmpMask.Canvas.FillRect(Rect(ColumnPoint[i].X, ColumnPoint[i].Y, ColumnPoint[i+1].X, Height));
    bmpMask.Canvas.FillRect(Rect(ColumnPoint[fCountColumn-1].X, ColumnPoint[i].Y, Width, Height));
    MaskBlt(Canvas.Handle, 0, 0, bmpBack.Width, bmpBack.Height, bmpBack.Canvas.Handle, 0, 0, bmpMask.Handle, 0, 0, MakeROP4(PATCOPY xor PATINVERT, SRCCOPY));
  end
  else
  if (fMode = vssmColumn) then
  begin
    bmpMask.Canvas.Brush.Color := clWhite;
    bmpMask.Canvas.FillRect(Rect(0, 0, bmpMask.Width, bmpMask.Height));

    bmpMask.Canvas.Brush.Color := clBlack;
    c := fCountColumn-1;
    for i := 0 to c do
      bmpMask.Canvas.FillRect(Rect(ColumnPoint[i].X, ColumnPoint[i].Y, ColumnPoint[i].X+fWidthColumn-fColumnIndention, Height));
//      bmpMask.Canvas.FillRect(Rect(ColumnPoint[i].X+4, ColumnPoint[i].Y, ColumnPoint[i+1].X, Height));
    MaskBlt(Canvas.Handle, 0, 0, bmpBack.Width, bmpBack.Height, bmpBack.Canvas.Handle, 0, 0, bmpMask.Handle, 0, 0, MakeROP4(PATCOPY xor PATINVERT, SRCCOPY));
  end;

end;

procedure TVisualSoundSpectr.PrepareToPaint;
var
  blueH, greenH, yelloyH, redH, h : Integer;
begin
  FreeAndNil(bmpMask);
  bmpMask := TBitmap.Create;
  bmpMask.PixelFormat := pf1bit;
  bmpMask.SetSize(Width, Height);
  bmpBack.SetSize(Width, Height);

  blueH := Round(Height * 0.1);
  greenH := Round(Height * 0.3);
  yelloyH := Round(Height * 0.3);
  redH := Round(Height * 0.3);

//������ �������� ���������
  h := 0;
  GradientFillCanvas(bmpBack.Canvas, clRed, clYellow, rect(0, h, bmpBack.Width, redH), gdVertical);

  Inc(h, redH);
  GradientFillCanvas(bmpBack.Canvas, clYellow, RGB(28, 255, 49), rect(0, h, bmpBack.Width, h+yelloyH), gdVertical);

  Inc(h, yelloyH);
  GradientFillCanvas(bmpBack.Canvas, RGB(28, 255, 49), RGB(23, 69, 174), rect(0, h, bmpBack.Width, h+greenH), gdVertical);

  Inc(h, greenH);
  GradientFillCanvas(bmpBack.Canvas, RGB(23, 69, 174), RGB(23, 69, 174), rect(0, h, bmpBack.Width, h+blueH), gdVertical);
end;

procedure TVisualSoundSpectr.SetCountColumn(const Value: Integer);
begin
  if (Value > 0)and(fCountColumn <> Value) then
  begin
    fCountColumn := Value;
    SetLength(ColumnAmpl, fCountColumn);
    SetLength(ColumnPoint, fCountColumn);
    CalcAndPaint;
  end;
end;

procedure TVisualSoundSpectr.SetFixedHeight(const Value: Boolean);
begin
  fFixedHeight := Value;
  CalcAndPaint;
end;

procedure TVisualSoundSpectr.SetMaxHeightColumn(const Value: Real);
begin
  if (fMaxHeightColumn > 0)and(fMaxHeightColumn <> Value) then
  begin
    fMaxHeightColumn := Value;
    CalcAndPaint;
  end;
end;

procedure TVisualSoundSpectr.setMode(const Value: vssMode);
begin
  fMode := Value;
  CalcAndPaint;
end;

procedure TVisualSoundSpectr.WMSize(var Message: TWMSize);
begin
  PrepareToPaint;
  CalcAndPaint;
end;

end.
