unit uSoundSpectrum;

interface

uses
  MMSystem, System.Classes, System.SysUtils, System.Variants, Winapi.Windows, Winapi.Messages,
  Vcl.Forms, Vcl.Graphics, Vcl.ExtCtrls,

  VclTee.TeeGDIPlus, VCLTee.TeEngine,
  VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, uVisualSound;

type
  TPointArr = array of TPoint;
  PPointArr = ^TPointArr;

  TBufD = array of Double;

  TData16S = array [0..1] of DWORD;
  PData16S = ^TData16S;

  TData16 = array [0..1] of SmallInt;
  PData16 = ^TData16;

  TOnSoundLvlNotify = procedure (Sender: TObject; lvl_Left, lvl_Right : Integer) of object;

  TSoundSpectrum = class(TObject)
    private
      WaveIn: hWaveIn;        //����� ��������� ����� ���������� �����.
      hBuf: THandle;          //����� ������ �������
      BufHead: TWaveHdr;      //��������� WAVEHDR ���������� ���������, ������������ ��� ������������� ������� ����� ������.
      fStop: boolean;         //���������� ��� ����� � �������� �������

      Fs : integer;           //������� �������������
      BitSample : Byte;       //����������� ����������� ���
      Ch : Byte;              //����� (1-����/2-������)
      Fmax : Word;            //������������ ������� �������
      F  : Word;              //������� ������� �������
      Ns : Cardinal;          //���������� ����� �������
      N  : Integer;           //���������� ��������

//      p1: TPointArr;          //����� ������� ������������������ ����� ����� ��� ����
//      p2: TPointArr;          //����� ������� ������������������ ������ �����

      //��� ��������� �������
      BufData: TBufD;                //������������� �����
      B, alpha, Wr, Wi, Ampl: TBufD;
      FOnSoundLvlNotify : TOnSoundLvlNotify;
      procedure Gercel(Data : Pointer);
      procedure GetSoundLvl(Data : Pointer);
    public
//      ch2 : TChart;
      SpectrGercel : Boolean;
      SoundLvl : Boolean;
//      PBLeft, PBRight, PBGercel, PBSoundLeft, PBSoundRight : TPaintBox;
      SoundLvlTimeLeft, SoundLvlTimeRight : TVisualSoundLevelTime;
      SoundLvlLeft, SoundLvlRight : TVisualSoundLevel;
      SoundSpectr : TVisualSoundSpectr;
      Constructor Create; overload;
      Destructor Destroy; overload;
      procedure Start(hWindow : HWND;  idDevice : Word; iFs : DWord; iCh : Byte; iBitSample :Byte = 16);
      procedure Stop;
      procedure OnWaveIn(var Msg: TMessage); message MM_WIM_DATA;
      procedure SetBitPerSampleOnlyTest(bits : Byte);
    published
      property BitPerSample : Byte read BitSample;
      property OnSoundLvlNotify : TOnSoundLvlNotify read FOnSoundLvlNotify write FOnSoundLvlNotify;
      property Channel : Byte read Ch;
  end;

implementation

{ TSoundSpectrum }

procedure TSoundSpectrum.SetBitPerSampleOnlyTest(bits: Byte);
begin
  BitSample := bits;
end;

procedure TSoundSpectrum.Start(hWindow : HWND; idDevice : Word; iFs : DWord; iCh : Byte; iBitSample :Byte = 16);
var
  FHead: TWaveFormatEx;    //��������� WAVEFORMATEX ���������� ������ �����������
  BufLen: word;            //������ ������ �������
  buf: pointer;
begin
  Fs := iFs;                //������� �������������
  BitSample := iBitSample;  //����������� ����������� ���
  Ch := iCh;                //����� (1-����/2-������)
//  Fs := 22050;             //������� �������������
//  BitSample := 16;         //����������� ����������� ���
//  Ch := 2;                 //����� (1-����/2-������)
  Fmax := 17000;           //������������ ������� �������
  F := 50;                 //������� ������� �������
  Ns := Fmax div F;        //���������� ����� �������
  N := Fs div F * 2;       //���������� ��������

  fStop := true;
  if fStop = True then begin
//�������� ��������� ������� ������
    with FHead do begin
//����� ������������ ����-���������� ���������
      wFormatTag := WAVE_FORMAT_PCM;
//����������� ����/������
      nChannels := Ch;
//�������������
      nSamplesPerSec := Fs;
//����������� ������
      wBitsPerSample := BitSample;
// ����� ���� � �������
      nBlockAlign := nChannels * (wBitsPerSample div 8);
// ������� �������� �������� ������ ����/���
      nAvgBytesPerSec := nSamplesPerSec * nBlockAlign;
// ��� ������� ��� ������ �� �����, ������� ��������
      cbSize := 0;
    end;
    WaveInOpen(Addr(WaveIn), idDevice, addr(FHead), hWindow, 0, CALLBACK_WINDOW);
    BufLen := FHead.nBlockAlign * N;
    hBuf := GlobalAlloc(GMEM_MOVEABLE and GMEM_SHARE, BufLen);
    Buf := GlobalLock(hBuf);
    with BufHead do begin
      lpData := Buf;
      dwBufferLength := BufLen;
      dwFlags:= WHDR_BEGINLOOP
    end;
    WaveInPrepareHeader(WaveIn, Addr(BufHead), sizeof(BufHead));
    WaveInAddBuffer(WaveIn, Addr(BufHead), sizeof(BufHead));

    if BitSample = 16 then
    begin
      SoundLvlLeft.Min := 0;
      SoundLvlLeft.Max := 32768;
      SoundLvlRight.Min := 0;
      SoundLvlRight.Max := 32768;

      SoundLvlTimeLeft.CountPoint := n;
      SoundLvlTimeRight.CountPoint := n;
    end;
//    SetLength(p1, n);
//    SetLength(p2, n);

    if SpectrGercel then
    begin
      SetLength(BufData, N);
      SetLength(B, N);
      SetLength(alpha, N);
      SetLength(Wr, N);
      SetLength(Wi, N);
      SetLength(Ampl, N);
      SoundSpectr.CountColumn := Ns;
    end;

    fStop := true;
    WaveInStart(WaveIn);
  end;
end;

procedure TSoundSpectrum.Stop;
begin
  if fStop = false then Exit;
  fStop := false;
  while not fStop do
    Application.ProcessMessages;
  WaveInReset(WaveIn);
  WaveInUnPrepareHeader(WaveIn, Addr(BufHead), sizeof(BufHead));
  WaveInClose(WaveIn);
  GlobalUnlock(hBuf);
  GlobalFree(hBuf);

//  p1 := nil;
//  p2 := nil;

  if SpectrGercel then
  begin
    BufData := nil;
    B := nil;
    alpha := nil;
    Wr := nil;
    Wi := nil;
    Ampl := nil;
  end;
  SpectrGercel := false;
end;

constructor TSoundSpectrum.Create;
begin
  inherited;
  fStop := false;
//  PBLeft := nil;
//  PBRight := nil;
  SoundLvlTimeLeft  := nil;
  SoundLvlTimeRight := nil;
  SpectrGercel := false;
  SoundLvl := false;
end;

destructor TSoundSpectrum.Destroy;
begin
  inherited;
  Stop;
end;

procedure TSoundSpectrum.Gercel(Data : Pointer);
var
  i,j,k: integer;
  Frq: Integer;
  Re, Im: Double;
  Data16 : PData16;
  Data16S : PData16S;
begin
  //����������� ������ � �����
  if (BitSample = 16)and(Ch = 1) then
  begin
    Data16 := Data;
    for i:=0 to N-1 do BufData[i] := Data16^[i];
  end
  else
  if (BitSample = 16)and(Ch = 2) then
  begin
    Data16S := Data;
    for i:=0 to N-1 do BufData[i] := SmallInt(LOWORD(Data16S^[i]));
  end
  else exit;

  //������ �� �������
  //��������������� ������ ����� � ���������� ������������� ��� ���� ��������
  Frq:=-100;         //��������� ������� ����� -100 ��.
  For i:=0 To N-1 do
  begin
    k := Trunc(N*Frq/Fs);           //��������� ����� ��������� ���������
    alpha[i]:=2*cos(2*Pi*(k/N));    //������ �����. �����
    wr[i] := cos(2*Pi*(k/N));       //���������� �����. �������� �����
    wi[i] := sin(2*Pi*(k/N));       //���������� �����. ������ �����
    Frq:=Frq+F;                     //�������� �� ��������� �������
  end;

  //���� ������� �� �������� �� 50 �� Fmax(10000) �� � �������� F(50) ��
  For j:=0 to Ns-1 do
  begin
  //��� ������ ������� ������ B[-1] = B[-2] = 0
    B[0] := BufData[0];
    B[1] := BufData[1]+alpha[j+1]*B[0];
  //���� ������� B
    For i:=2 to N-1 do
      B[i] := BufData[i]+alpha[j+2]*B[i-1]-B[i-2];
  //�������� � ������ ����� ������������� �������
    Wr[j]:= B[N-1]*wr[j]-B[N-2];
    Wi[j]:= B[N-1]*wi[j];
  //��������� �� �������
    Re:=Wr[j]*Wr[j];
    Im:=Wi[j]*Wi[j];
  //������� � �������������� ��������� ���������
    Ampl[j]:=(SQRT(Re+Im))/150000;
    If j=0 Then Ampl[j]:=0;
  end;

//   Ch2.Series[0].Clear;
  //������� ������ �� ������
  For i:=0 to Ns-1 do
  begin
//    If Ampl[i]<2 Then Ampl[i]:= 0.5;
//    Ch2.Series[0].AddXY(i*F,Ampl[i]);
    SoundSpectr.ColumnAmpl[i] := Ampl[i];
  end;
  SoundSpectr.CalcAndPaint;
end;

procedure TSoundSpectrum.GetSoundLvl(Data: Pointer);
var
  maxLeft, maxRight, l1 : Integer;
  Data16 : PData16;
  Data16S : PData16S;
  i, q : integer;
begin
  if (BitSample = 16)and(Ch = 1) then
  begin
    Data16 := Data;
    maxLeft := 0;
    for i:=0 to N-1 do
//      if Abs(Data16^[i] - 32767) > maxLeft then maxLeft := Abs(Data16^[i] - 32767);
      if Abs(Data16^[i]) > maxLeft then maxLeft := Abs(Data16^[i]);

    SoundLvlLeft.Position := maxLeft;
    if Assigned(FOnSoundLvlNotify) then FOnSoundLvlNotify(Self, maxLeft, -1);

//    with PBSoundLeft do
//    begin
//      Canvas.Brush.Color:=clWhite;
//      Canvas.FillRect(Canvas.ClipRect);
//
//      Canvas.Rectangle(0, Height, Width, Height);
//    end;
  end
  else
  if (BitSample = 16)and(Ch = 2) then  // 32767
  begin
    Data16S := Data;
    maxLeft := 0;
    maxRight := 0;
    for i:=0 to N-1 do
      begin
        q := Abs(SmallInt(LOWORD(Data16S^[i])));
        if q > maxLeft  then maxLeft := q;

        q := SmallInt(HIWORD(Data16S^[i]));
        if q > maxRight then maxRight := q;
      end;

    SoundLvlLeft.Position := maxLeft;
    SoundLvlRight.Position := maxRight;

    if Assigned(FOnSoundLvlNotify) then FOnSoundLvlNotify(Self, maxLeft, maxRight);
//    with PBSoundLeft do
//    begin
//      Canvas.Brush.Color:=clWhite;
//      Canvas.FillRect(Canvas.ClipRect);
//      Canvas.Brush.Color:=clBlack;
//      Canvas.Rectangle(0, PBSoundLeft.Height - round(maxLeft/32768*PBSoundLeft.Height), Width, Height);
//    end;
//
//    with PBSoundRight do
//    begin
//      Canvas.Brush.Color:=clWhite;
//      Canvas.FillRect(Canvas.ClipRect);
//      Canvas.Brush.Color:=clBlack;
//      Canvas.Rectangle(0, PBSoundLeft.Height - round(maxLeft/32768*PBSoundLeft.Height), Width, Height);
//    end;
  end;
end;

procedure TSoundSpectrum.OnWaveIn(var Msg: TMessage);
var
//  i : integer;

  i,j,k: integer;
  BufData: TBufD;                //������������� �����
  CapData : PData16;             //����� ������� �����

//	data16: PData16;
  data16S: PData16S;
	h, h2: integer;
	XScale, YScale: single;
  d : TData16;
begin
//������ �����
  CapData := PData16(PWaveHdr(Msg.lParam)^.lpData);

//������ �� ��������� �������
  if SpectrGercel then Gercel(PWaveHdr(Msg.lParam)^.lpData);

//������� ������������ ������� �������
  if SoundLvl then GetSoundLvl(PWaveHdr(Msg.lParam)^.lpData);

//����� ������
  if BitSample = 16 then
  if Ch = 1 then //mono
  begin
    h := SoundLvlTimeLeft.Height;
    XScale := SoundLvlTimeLeft.Width / (N-1);
		YScale := h / (1 shl 16);
    for i := 0 to N - 1 do
      SoundLvlTimeLeft.Points[i]  := Point(round(i * XScale), round(h / 2 - CapData^[i] * YScale));
    SoundLvlTimeLeft.Repaint;
//    if PBLeft <> nil then
//    begin
//      h := PBLeft.Height;
//      XScale := PBLeft.Width / (N-1);
//      YScale := h / (1 shl 16);
//      for i := 0 to N-1 do
//        p1[i] := Point(round(i * XScale), round(h / 2 - CapData^[i] * YScale));
//
//      with PBLeft.Canvas do
//      begin
//        Brush.Color := clWhite;
//        FillRect(ClipRect);
//        MoveTo(0, PBLeft.Height div 2);
//        LineTo(PBLeft.Width, PBLeft.Height div 2);
//        Polyline(p1);
//      end;
//    end;
  end
  else
  if Ch = 2 then //stereo
  begin
//    h := PBLeft.Height;
    Data16S := PData16S(PWaveHdr(Msg.lParam)^.lpData);

    h := SoundLvlTimeLeft.Height;
    XScale := SoundLvlTimeLeft.Width / (N-1);
		YScale := h / (1 shl 16);
		for i := 0 to N - 1 do
    begin
      SoundLvlTimeLeft.Points[i]  := Point(round(i * XScale), round(h / 2 - SmallInt(LOWORD(data16S^[i])) * YScale));
      SoundLvlTimeRight.Points[i] := Point(round(i * XScale), round(h / 2 - SmallInt(HIWORD(data16S^[i])) * YScale));
    end;
    SoundLvlTimeLeft.Repaint;
    SoundLvlTimeRight.Repaint;
//
//    with PBLeft.Canvas do
//      begin
//        Brush.Color := clWhite;
//        FillRect(ClipRect);
//        MoveTo(0, PBLeft.Height div 2);
//        LineTo(PBLeft.Width, PBLeft.Height div 2);
//        Polyline(p1);
//      end;
//
//      with PBRight.Canvas do
//      begin
//        Brush.Color := clWhite;
//        FillRect(ClipRect);
//        MoveTo(0, PBRight.Height div 2);
//        LineTo(PBRight.Width, PBRight.Height div 2);
//        Polyline(p2);
//      end;
  end;       {}

//���������� ����� ����� �������
  if fStop = True then WaveInAddBuffer(WaveIn, PWaveHdr(Msg.lParam),SizeOf(TWaveHdr))
                  else fStop := True;
end;

end.
