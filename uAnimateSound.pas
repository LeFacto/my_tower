unit uAnimateSound;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Math, LedDisplay, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Samples.Spin, Vcl.Buttons, System.Generics.Collections;

type

  TAnimateEffect = class(TObject)
  private
    FAnimMask : array of Word;
  public
    itemIndexCaption : Integer;
    options : Boolean;
    Caption : String;
    CaptionOptions : String;
    Channel : Byte;
    Filter : Byte;
    defaultHead : byte;
    constructor Create;
    destructor Destroy; override;
    procedure Animate(lvlLeft : Integer; lvlRight : integer = -1); virtual; abstract;
    procedure ReName; virtual; abstract;
    procedure CreateAnimMask(shift : Word; clockwise : Boolean = true);
    procedure PrepareToAnim; virtual; abstract;
    procedure ShowOptions; virtual; abstract;
  end;

const AnimShiftString : array [0..3] of String = ('������ ������� �������', '�� ������� �������', '�� ���� � �����', '�� ����� � ����');
type
  TAnimShift = (asLeft, asRight, asUp, asDown);
  TAnimEffectSimple_Mono = class (TAnimateEffect)
  public
    animShift : TAnimShift;
    procedure Animate(lvlLeft : Integer; lvlRight : integer = -1); Override;
    procedure ReName; Override;
    procedure PrepareToAnim; override;
    procedure ShowOptions; override;
  end;

const AnimEffectOnlyBorderString : array [0..1] of String = ('�� ���� � �����', '�� ����� � ����');
type
  TAnimEffectOnlyBorder = (obUp, obDown);
  TAnimEffectOnlyBorder_Mono = class (TAnimateEffect)
  private
  public
    animShift : TAnimEffectOnlyBorder;
    procedure Animate(lvlLeft : Integer; lvlRight : integer = -1); Override;
    procedure ReName; Override;
    procedure PrepareToAnim; override;
    procedure ShowOptions; override;
  end;

  TAnimStairs = (asMount, asIncClockWise, asIncNoClockWise);
var AnimStairsName : array [0..2] of String  = ('� ���� ����', '���������� �� ������� ��������', '���������� ������ ������� ��������');
type
  TAnimEffectStairs_Mono = class (TAnimateEffect)
  private
    FCenter : Word; //����� ��������
    FStepY : real; //��� �� Y ������������� ���� �� �
  public
    AnimStairs : TAnimStairs;
    procedure Animate(lvlLeft : Integer; lvlRight : integer = -1); Override;
    procedure ReName; Override;
    procedure PrepareToAnim; override;
    procedure ShowOptions; override;
  end;

  TAnimEffectOnlyBorderShift_Mono = class (TAnimateEffect)
  private
//    animShift : TAnimShift;
    FShiftTek, FShift, FShift1, FShift2 : Word; //��� ���� � ��������;
  public
    procedure Animate(lvlLeft : Integer; lvlRight : integer = -1); Override;
    procedure ReName; Override;
    procedure PrepareToAnim; override;
  end;

const AnimSimpleString : array [0..1] of String = ('�� ������', '�� �����');
type
  TAnimSimpleStereo = (assCenter, assFringe);
  TAnimEffectSimple_Stereo = class (TAnimateEffect)
  public
    AnimSimple : TAnimSimpleStereo;
    procedure Animate(lvlLeft : Integer; lvlRight : integer = -1); Override;
    procedure ReName; Override;
    procedure PrepareToAnim; override;
    procedure ShowOptions; override;
  end;

const AnimScanShift_String : array [0..2] of String =('������ �������', '���� �� ���������', '���� �� ��������� ������ �������');
type
  TAnimScanShift = (assClear, assFull, assDots);
  TAnimEffectScanShift_Mono = class (TAnimateEffect)
  private
    CurCol : Word;
    lvlCols : array of Word;
    destructor Destroy; override;
  public
    putDown : boolean; //�������� �� ���� ��������
    animShift : TAnimShift;
    AnimScanShift : TAnimScanShift;
    procedure Animate(lvlLeft : Integer; lvlRight : integer = -1); Override;
    procedure ReName; Override;
    procedure PrepareToAnim; override;
    procedure ShowOptions; override;
  end;

implementation


  { TAnimEffectSimple_Mono }

uses g_Form, uVisualSound;

procedure TAnimEffectSimple_Mono.Animate(lvlLeft, lvlRight: integer);
var
  d, i, j, r, c, k, ii : Integer;
  lvlUp, lvlWidth : Integer;
begin
  if gForm.SoundSpectrum.BitPerSample = 16 then d := 32767
    else if gForm.SoundSpectrum.BitPerSample = 8 then d := 128;

  lvlLeft := ConToReal(lvlLeft, 0, d, 0, gForm.LedDisplay.Rows);

  c := gForm.LedDisplay.Cols-1;
  r := gForm.LedDisplay.Rows-1;

  if animShift = asUp then  //�� ���� � ����� ��������
  begin
    for i := 0 to r do
      if i < gForm.LedDisplay.Rows - lvlLeft then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false
                                           else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true;
  end
  else
  if animShift = asDown then  //�� ����� � ���� ��������
  begin
    for i := 0 to r do
    if i >= lvlLeft then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false
                    else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true;
  end
  else
  if animShift = asLeft then //������ ������� �������
  begin
    for i := 0 to r do
      for j := 0 to lvlLeft do gForm.LedDisplay.Leds[i, FAnimMask[j]].Vis := true;

    for i := 0 to r do
      for j := lvlLeft+1 to c do gForm.LedDisplay.Leds[i, FAnimMask[j]].Vis := false;
  end
  else
  if animShift = asRight then
  begin
    for i := 0 to r do
    for j := 0 to lvlLeft do gForm.LedDisplay.Leds[i, FAnimMask[j]].Vis := true;

    for i := 0 to r do
    for j := lvlLeft+1 to c do gForm.LedDisplay.Leds[i, FAnimMask[j]].Vis := false;
  end;

  gForm.LedDisplay.PaintOnlyLed;
  gForm.SendStateLedTower(nil);
end;

procedure TAnimEffectSimple_Mono.PrepareToAnim;
begin
  case animShift of
    asLeft  : CreateAnimMask(gForm.LedDisplay.Cols shr 1, false);
    asRight : CreateAnimMask(0, true);
    asUp    : defaultHead := 0;
    asDown  : defaultHead := MAXBYTE;
  end;
end;

procedure TAnimEffectSimple_Mono.ReName;
begin
  Caption := '������� ';
  CaptionOptions := AnimShiftString[ord(animShift)];
end;

procedure TAnimEffectSimple_Mono.ShowOptions;
var
  form : TForm;
  p : TPoint;
  radio_b : TRadioGroup;
  q : Integer;
  btn : TButton;
  i : Word;
begin
  form := TForm.Create(nil);
  with form do
  begin
    Caption := '�����';
    Position := poDesigned;
    BorderStyle := bsToolWindow;
    p := gForm.OptionsBtn.ClientToScreen(Point(0, 0));
    Left := p.X;
    Top  := p.Y + gForm.OptionsBtn.Height;

    radio_b := TRadioGroup.Create(form);
    radio_b.Parent := form;
    radio_b.Left := 0;
    radio_b.Top := 0;
    for i := 0 to High(AnimShiftString) do radio_b.Items.Add(AnimShiftString[i]);
    radio_b.Height := 100;
    radio_b.ItemIndex := Ord(animShift);

    btn := TButton.Create(form);
    btn.Parent := form;
    btn.ModalResult := mrClose;
    btn.Caption := 'Ok';
    btn.Top := radio_b.Height + 1;
    btn.Left := radio_b.Width div 2 - btn.Width div 2;
    Form.AutoSize := true;

    ShowModal;
  end;

  animShift := TanimShift(radio_b.ItemIndex);

  PrepareToAnim;
  ReName;
  q := gForm.AnimateComboBox.ItemIndex;
  gForm.AnimateComboBox.Items[itemIndexCaption] := Caption;
  gForm.AnimateComboBox.ItemIndex := q;

  radio_b.Free;
  btn.Free;
  form.Free;
end;

{ TAnimateEffect }

constructor TAnimateEffect.Create;
begin
  inherited;
  defaultHead := 0;
end;

procedure TAnimateEffect.CreateAnimMask(shift: Word; clockwise: Boolean);
var
  i, c : word;
begin
  SetLength(FAnimMask, 0);
  SetLength(FAnimMask, gForm.LedDisplay.Rows);

  c := 0;
  i := shift;

  while gForm.LedDisplay.Rows <> c do
  begin
    FAnimMask[c] := i;
    if clockwise then
    begin
      Inc(i);
      if i = gForm.LedDisplay.Rows then i := 0;
    end
    else
    begin
      Dec(i);
      if i = 65535 then i := gForm.LedDisplay.Rows-1;
    end;
    Inc(c);
  end;
end;

destructor TAnimateEffect.Destroy;
begin
  SetLength(FAnimMask, 0);
  inherited;
end;

{ TAnimEffectOnlyBorder_Mono }

procedure TAnimEffectOnlyBorder_Mono.Animate(lvlLeft, lvlRight: integer);
var
  d, i, j, r, c : Integer;
begin
  if gForm.SoundSpectrum.BitPerSample = 16 then d := 32767
    else if gForm.SoundSpectrum.BitPerSample = 8 then d := 128;
  lvlLeft := ConToReal(lvlLeft, 0, d, 0, gForm.LedDisplay.Rows);

  c := gForm.LedDisplay.Cols-1;
  r := gForm.LedDisplay.Rows-1;

  if animShift = obUp then //�� ���� � ����� �������� �� ������ ������� ������� � ������
  begin
    if lvlLeft >= 2 then
    begin
      lvlLeft := r-lvlLeft+1;
      for j := 0 to c do gForm.LedDisplay.Leds[r, j].Vis := true;
      for i := 0 to r-1 do
        if i = lvlLeft then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true
                   else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
    end
    else
    if lvlLeft = 0 then
    begin
      for i := 0 to r do
        for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
    end
    else
    if lvlLeft = 1 then
    begin
      for j := 0 to c do gForm.LedDisplay.Leds[r, j].Vis := true;
      for i := 0 to r-1 do
        for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
    end;
  end
  else
  if animShift = obDown then //�� ����� � ���� �������� ������ �������
  begin
    if lvlLeft >= 2 then
    begin
      Dec(lvlLeft);
      for j := 0 to c do gForm.LedDisplay.Leds[0, j].Vis := true;
      for i := 1 to r do
        if i = lvlLeft
          then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true
                   else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
    end
    else
    if lvlLeft = 0 then
    begin
      for i := 0 to r do
        for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
    end
    else
    if lvlLeft = 1 then
    begin
      for j := 0 to c do gForm.LedDisplay.Leds[0, j].Vis := true;
      for i := 1 to r do
        for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
    end;
  end;

  gForm.LedDisplay.PaintOnlyLed;
  gForm.SendStateLedTower(nil);
end;

procedure TAnimEffectOnlyBorder_Mono.PrepareToAnim;
begin
  defaultHead := 0;
end;

procedure TAnimEffectOnlyBorder_Mono.ReName;
begin
  Caption := '������ ������� ';
  CaptionOptions := AnimEffectOnlyBorderString[ord(animShift)];
end;

procedure TAnimEffectOnlyBorder_Mono.ShowOptions;
var
  form : TForm;
  p : TPoint;
  radio_b : TRadioGroup;
  q : Integer;
  btn : TButton;
  i : Word;
begin
  form := TForm.Create(nil);
  with form do
  begin
    Caption := '�����';
    Position := poDesigned;
    BorderStyle := bsToolWindow;
    p := gForm.OptionsBtn.ClientToScreen(Point(0, 0));
    Left := p.X;
    Top  := p.Y + gForm.OptionsBtn.Height;

    radio_b := TRadioGroup.Create(form);
    radio_b.Parent := form;
    radio_b.Left := 0;
    radio_b.Top := 0;
    for i := 0 to High(AnimEffectOnlyBorderString) do radio_b.Items.Add(AnimEffectOnlyBorderString[i]);
    radio_b.Height := 60;
    radio_b.ItemIndex := Ord(animShift);

    btn := TButton.Create(form);
    btn.Parent := form;
    btn.ModalResult := mrClose;
    btn.Caption := 'Ok';
    btn.Top := radio_b.Height + 1;
    btn.Left := radio_b.Width div 2 - btn.Width div 2;
    Form.AutoSize := true;

    ShowModal;
  end;

  animShift := TAnimEffectOnlyBorder(radio_b.ItemIndex);

  PrepareToAnim;
  ReName;
  q := gForm.AnimateComboBox.ItemIndex;
  gForm.AnimateComboBox.Items[itemIndexCaption] := Caption;
  gForm.AnimateComboBox.ItemIndex := q;

  radio_b.Free;
  btn.Free;
  form.Free;
end;

{ TAnimEffectStairs_Mono }

procedure TAnimEffectStairs_Mono.Animate(lvlLeft, lvlRight: integer);
var
  d, i, j, r, c, k, h : Integer;
  lvlUp, lvlWidth : Integer;
begin
  if gForm.SoundSpectrum.BitPerSample = 16 then d := 32767
    else if gForm.SoundSpectrum.BitPerSample = 8 then d := 128;

  lvlUp := ConToReal(lvlLeft, 0, d, 0, gForm.LedDisplay.Rows);
  c := gForm.LedDisplay.Cols-1;
  r := gForm.LedDisplay.Rows-1;

  if AnimStairs = asMount then
  begin
//    for i := 0 to r do
//      for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
    h := gForm.LedDisplay.Rows - lvlUp; //����� ������� ����� (�����)
    for j := 0 to c do //���� �� ��������
    begin
      //������� ������ ������ � �������, ���� ���� �� ����� � ����
      if j <= FCenter then k := Round(h + (j) * FStepY)
                      else k := Round(h + (c+1 - j) * FStepY);
      if k > r then k := r; //���� ����� �� �����
      for i := 0 to k do gForm.LedDisplay.Leds[i, FAnimMask[j]].Vis := false;
      if k <> r then //���� �� ��� ������
        for i := k to r do gForm.LedDisplay.Leds[i, FAnimMask[j]].Vis := true;
    end;
  end
  else
//  if (AnimStairs = asIncClockWise)or(AnimStairs = asIncNoClockWise) then
  begin
    lvlWidth := ConToReal(lvlLeft, 0, d, 0, gForm.LedDisplay.Cols-1);

    for i := 0 to r do
      for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;

    for j := 0 to lvlWidth do
      for i := r downto r-j do gForm.LedDisplay.Leds[i, FAnimMask[j]].Vis := true;
  end;

  gForm.LedDisplay.PaintOnlyLed;
  gForm.SendStateLedTower(nil);
end;

procedure TAnimEffectStairs_Mono.PrepareToAnim;
begin
  defaultHead := MAXBYTE;
  case  AnimStairs of
    asIncClockWise:  CreateAnimMask(0, true);
    asIncNoClockWise: CreateAnimMask(0, false);
    asMount:
    begin
      FCenter := gForm.LedDisplay.Rows shr 1;
      FStepY := gForm.LedDisplay.Rows / (gForm.LedDisplay.Cols shr 1);
      CreateAnimMask(gForm.LedDisplay.Rows - (FCenter shr 1), false);
    end;
  end;
end;

procedure TAnimEffectStairs_Mono.ReName;
begin
  Caption := '��������� ';
  CaptionOptions := AnimStairsName[ord(AnimStairs)];
end;

procedure TAnimEffectStairs_Mono.ShowOptions;
var
  form : TForm;
  p : TPoint;
  radio_b : TRadioGroup;
  q : Integer;
  btn : TButton;
  i : Word;
begin
  form := TForm.Create(nil);
  with form do
  begin
    Caption := '�����';
    Position := poDesigned;
    BorderStyle := bsToolWindow;
    p := gForm.OptionsBtn.ClientToScreen(Point(0, 0));
    Left := p.X;
    Top  := p.Y + gForm.OptionsBtn.Height;

    radio_b := TRadioGroup.Create(form);
    radio_b.Parent := form;
    radio_b.Left := 0;
    radio_b.Top := 0;
    for i := 0 to High(AnimStairsName) do radio_b.Items.Add(AnimStairsName[i]);
    radio_b.Height := 80;
    radio_b.Width := 230;
    radio_b.ItemIndex := Ord(AnimStairs);

    btn := TButton.Create(form);
    btn.Parent := form;
    btn.ModalResult := mrClose;
    btn.Caption := 'Ok';
    btn.Top := radio_b.Height + 1;
    btn.Left := radio_b.Width div 2 - btn.Width div 2;
    Form.AutoSize := true;

    ShowModal;
  end;

  AnimStairs := TAnimStairs(radio_b.ItemIndex);

  PrepareToAnim;
  ReName;
  q := gForm.AnimateComboBox.ItemIndex;
  gForm.AnimateComboBox.Items[itemIndexCaption] := Caption;
  gForm.AnimateComboBox.ItemIndex := q;

  radio_b.Free;
  btn.Free;
  form.Free;
end;

{ TAnimEffectSimple_Stereo }

procedure TAnimEffectSimple_Stereo.Animate(lvlLeft, lvlRight: integer);
var
  d, i, j, r, c, center : Integer;
begin
  if gForm.SoundSpectrum.BitPerSample = 16 then d := 32768 else
    if gForm.SoundSpectrum.BitPerSample = 8 then d := 128;

  if gForm.LedDisplay.Rows mod 2 = 0 then center := gForm.LedDisplay.Rows shr 1 else center := (gForm.LedDisplay.Rows shr 1)+1;
  lvlLeft := ConToReal(lvlLeft,  0, d, 0, gForm.LedDisplay.Rows-center);
  lvlRight := ConToReal(lvlRight, 0, d-1, 0, gForm.LedDisplay.Rows-center);

  c := gForm.LedDisplay.Cols-1;
  r := gForm.LedDisplay.Rows-1;

  if AnimSimple = assCenter then
  begin
    for i := 0 to center-1 do
      if i < center-lvlLeft then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false
                            else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true;
    for i := center to r do
      if i >= center+lvlRight then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false
                              else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true;
  end
  else
  begin
    for i := 0 to center-1 do
      if i >= lvlLeft then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false
                      else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true;
    for i := center to r do
      if i <= r-lvlRight then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false
                              else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true;
  end;

  gForm.LedDisplay.PaintOnlyLed;
  gForm.SendStateLedTower(nil);
end;

procedure TAnimEffectSimple_Stereo.PrepareToAnim;
begin
  defaultHead := MAXBYTE;
end;

procedure TAnimEffectSimple_Stereo.ReName;
begin
  Caption := '������� ';
  CaptionOptions := AnimSimpleString[Ord(AnimSimple)];
end;

procedure TAnimEffectSimple_Stereo.ShowOptions;
var
  form : TForm;
  p : TPoint;
  radio_b : TRadioGroup;
  q : Integer;
  btn : TButton;
  i : Word;
begin
  form := TForm.Create(nil);
  with form do
  begin
    Caption := '�����';
    Position := poDesigned;
    BorderStyle := bsToolWindow;
    p := gForm.OptionsBtn.ClientToScreen(Point(0, 0));
    Left := p.X;
    Top  := p.Y + gForm.OptionsBtn.Height;

    radio_b := TRadioGroup.Create(form);
    radio_b.Parent := form;
    radio_b.Left := 0;
    radio_b.Top := 0;
    for i := 0 to High(AnimSimpleString) do radio_b.Items.Add(AnimSimpleString[i]);
    radio_b.Height := 60;
    radio_b.Width := 100;
    radio_b.ItemIndex := Ord(AnimSimple);

    btn := TButton.Create(form);
    btn.Parent := form;
    btn.ModalResult := mrClose;
    btn.Caption := 'Ok';
    btn.Top := radio_b.Height + 1;
    btn.Left := radio_b.Width div 2 - btn.Width div 2;
    Form.AutoSize := true;

    ShowModal;
  end;

  AnimSimple := TAnimSimpleStereo(radio_b.ItemIndex);

  PrepareToAnim;
  ReName;
  q := gForm.AnimateComboBox.ItemIndex;
  gForm.AnimateComboBox.Items[itemIndexCaption] := Caption;
  gForm.AnimateComboBox.ItemIndex := q;

  radio_b.Free;
  btn.Free;
  form.Free;
end;

{ TAnimEffectOnlyBorderShift_Mono }

procedure TAnimEffectOnlyBorderShift_Mono.Animate(lvlLeft, lvlRight: integer);
var
  d, i, j, r, c : Integer;
label
  l1;
begin
  if gForm.SoundSpectrum.BitPerSample = 16 then d := 32766
    else if gForm.SoundSpectrum.BitPerSample = 8 then d := 128;
  lvlLeft := ConToReal(lvlLeft, 0, d, 0, gForm.LedDisplay.Rows);

  c := gForm.LedDisplay.Cols-1;
  r := gForm.LedDisplay.Rows-1;

  //���� ������������ ������ ���������� ���������� ������������� �������� ������� ������ ������
  if gForm.FilterBox.Checked then
    if gForm.Filter.Skiped <> gForm.Filter.CountSkiped then goto l1;
  //������� ������
  if ((lvlLeft * 100) / gForm.LedDisplay.Rows) >= 80 then
  begin
    Inc(FShift1);
    if FShift1 = gForm.LedDisplay.Rows then FShift1 := 0;

    Inc(FShift2);
    if FShift2 = gForm.LedDisplay.Rows then FShift2 := 0;

    Inc(FShiftTek);
    if FShiftTek = FShift then FShiftTek := 0;
  end;

  l1:
  if lvlLeft >= 2 then
  begin
    lvlLeft := r-lvlLeft+1;

    for j := 0 to FShift-1 do gForm.LedDisplay.Leds[r, j].Vis := false;
    i := 0;
    for j := FShiftTek to c do //������ ������
    begin
      if i = 0 then gForm.LedDisplay.Leds[r, j].Vis := true else gForm.LedDisplay.Leds[r, j].Vis := false;
      Inc(i);
      if i = FShift then i := 0;
    end;


    for i := 0 to r-1 do
      if i = lvlLeft then for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := true
                 else for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;

    for i := lvlLeft+1 to r-1 do
    begin
      gForm.LedDisplay.Leds[i, FShift1].Vis := true;
      gForm.LedDisplay.Leds[i, FShift2].Vis := true;
    end;
  end
  else
  if lvlLeft = 0 then
  begin
    for i := 0 to r do
      for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
  end
  else
  if lvlLeft = 1 then
  begin
    for j := 0 to c do gForm.LedDisplay.Leds[r, j].Vis := true;

    for i := 0 to r-1 do
      for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;
  end;

  gForm.LedDisplay.PaintOnlyLed;
  gForm.SendStateLedTower(nil);
end;

procedure TAnimEffectOnlyBorderShift_Mono.PrepareToAnim;
begin
  defaultHead := 0;

  FShiftTek := 0;
  FShift := gForm.LedDisplay.Cols shr 2;
  FShift1 := 0;
  FShift2 := gForm.LedDisplay.Cols shr 1;
end;

procedure TAnimEffectOnlyBorderShift_Mono.ReName;
begin
  Caption := '������ ������� �� ������� ';
end;

{ TAnimEffectScanShift }

procedure TAnimEffectScanShift_Mono.Animate(lvlLeft, lvlRight: integer);
var
  i, c, j, r, q : Word;
  d : integer;
label
  l1;
begin
  if gForm.SoundSpectrum.BitPerSample = 16 then d := 32767
    else if gForm.SoundSpectrum.BitPerSample = 8 then d := 128;

  lvlLeft := ConToReal(lvlLeft, 0, d, 0, gForm.LedDisplay.Rows);

  lvlLeft := gForm.LedDisplay.Rows - lvlLeft; //��������������
  lvlCols[CurCol] := lvlLeft; //���������� ������ ��������� ������� � ����� ������ �����


  c := gForm.LedDisplay.Cols-1;
  r := gForm.LedDisplay.Rows-1;

//������� ������� ��������
  for i := 0 to r do
    for j := 0 to c do gForm.LedDisplay.Leds[i, j].Vis := false;

//����� �������� �� �������� �������
  for i := 0 to r do
    //�� ���� � �����
    if lvlLeft <= i then gForm.LedDisplay.Leds[i, FAnimMask[CurCol]].Vis := true
                    else gForm.LedDisplay.Leds[i, FAnimMask[CurCol]].Vis := false;

  if AnimScanShift = assDots then
  begin
  //����� �������� �� ��������� �������, ������ �����
    for j := 0 to c do
      if lvlCols[j] <= c then
      if j <> CurCol then gForm.LedDisplay.Leds[// gForm.LedDisplay.Rows - lvlCols[j],
        lvlCols[j],
          FAnimMask[j]].Vis := true;
  end
  else
  if AnimScanShift = assFull then
  begin
    //����� �������� �� ��������� �������

//    for i := 0 to r do
//    begin
    for j := 0 to c do
      if j <> CurCol then
      begin
        q := lvlCols[j];
        for i := r downto q do
          gForm.LedDisplay.Leds[i, FAnimMask[j]].Vis := true;
      end;

//    ������� ������� ����� ����� ��������
    q := CurCol;
    if q = c then q := 0 else Inc(q); //��������� �������
    for i := 0 to r do gForm.LedDisplay.Leds[i, FAnimMask[q]].Vis := false;
  end;

//���� ������������ ������ ���������� ���������� ������������� �������� ������� ������ ������
  if gForm.FilterBox.Checked then
    if gForm.Filter.Skiped <> gForm.Filter.CountSkiped then goto l1;

  //����� �������� ����
  if putDown then
  begin
    for j := 0 to c do Inc(lvlCols[j]);
  end;

  if CurCol = c then CurCol := 0 else Inc(CurCol); //��������� �������

l1:
  gForm.LedDisplay.PaintOnlyLed;
  gForm.SendStateLedTower(nil);
end;

destructor TAnimEffectScanShift_Mono.Destroy;
begin
  SetLength(lvlCols, 0);
  inherited;
end;

procedure TAnimEffectScanShift_Mono.PrepareToAnim;
var
  i : Word;
begin
  defaultHead := MAXBYTE;
  CurCol := 0;
  SetLength(lvlCols, gForm.LedDisplay.Cols);
  for i := 0 to gForm.LedDisplay.Cols do lvlCols[i] := gForm.LedDisplay.Rows+1;

  if animShift = asLeft then CreateAnimMask(gForm.LedDisplay.Cols shr 1, false);
  if animShift = asRight then CreateAnimMask(0, true);
end;

procedure TAnimEffectScanShift_Mono.ReName;
begin
  Caption := '������� �� ������� ';
  case animShift of
    asLeft :  CaptionOptions :=  '������ ������� �������';
    asRight : CaptionOptions := '�� ������� �������';
  end;
end;

procedure TAnimEffectScanShift_Mono.ShowOptions;
var
  form : TForm;
  p : TPoint;
  radio_b : TRadioGroup;
  radio_g : TRadioGroup;
  check_b : TCheckBox;
  q : Integer;
  btn : TButton;
  i : Integer;
begin
  form := TForm.Create(nil);
  with form do
  begin
    Caption := '�����';
    Position := poDesigned;
    BorderStyle := bsToolWindow;
    p := gForm.OptionsBtn.ClientToScreen(Point(0, 0));
    Left := p.X;
    Top  := p.Y + gForm.OptionsBtn.Height;

    radio_b := TRadioGroup.Create(form);
    radio_b.Parent := form;
    radio_b.Left := 0;
    radio_b.Top := 0;
    radio_b.Items.Add('�� ������� �������');
    radio_b.Items.Add('������ ������� �������');
    radio_b.Height := 60;
    radio_b.Width := 220;

    case animShift of
      asRight : radio_b.ItemIndex := 0;
      asLeft  : radio_b.ItemIndex := 1;
    end;

    radio_g := TRadioGroup.Create(form);
    radio_g.Parent := form;
    radio_g.Left := 0;
    radio_g.Top := radio_b.Top + radio_b.Height + 8;
    radio_g.Height := 80;
    radio_g.Width := 220;
    for i := 0 to 2 do radio_g.Items.Add(AnimScanShift_String[i]);
    radio_g.ItemIndex := Ord(AnimScanShift);

    check_b := TCheckBox.Create(form);
    check_b.Parent := form;
    check_b.Left := 8;
    check_b.Top := radio_g.Top + radio_g.Height + 3;
    check_b.Width := 220;
    check_b.Caption := '�������� ���� ��������';
    check_b.Checked := putDown;

    btn := TButton.Create(form);
    btn.Parent := form;
    btn.ModalResult := mrClose;
    btn.Caption := 'Ok';
    btn.Top := check_b.Top + check_b.Height + 3;
    btn.Left := radio_b.Width div 2 - btn.Width div 2;
    Form.AutoSize := true;


    ShowModal;
  end;

  // ��������� ������ ��������
  case radio_b.ItemIndex of
    0 : animShift := asRight;
    1 : animShift := asLeft;
  end;

  AnimScanShift := TAnimScanShift(radio_g.ItemIndex);

  PrepareToAnim;
  ReName;
  q := gForm.AnimateComboBox.ItemIndex;
  gForm.AnimateComboBox.Items[itemIndexCaption] := Caption;
  gForm.AnimateComboBox.ItemIndex := q;

  putDown := check_b.Checked;

  radio_b.Free;
  radio_g.Free;
  btn.Free;
  form.Free;
end;


end.
