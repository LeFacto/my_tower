unit g_Form;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Math, LedDisplay, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Samples.Spin, System.Win.TaskbarCore, Vcl.Taskbar, Vcl.Buttons, SynEditHighlighter, SynHighlighterGeneral,
  SynEdit, Vcl.Themes, Vcl.Styles, SynHighlighterDWS, System.ImageList, Vcl.ImgList,
  SynHighlighterPas, SynEditTypes, System.Actions, Vcl.ActnList, Vcl.ToolWin, uParserScript.IDE_Debuger,
  Vcl.WinXCtrls, BCPort, MMSystem, uVisualSound, uSoundSpectrum, System.Generics.Collections, uAnimateSound, MMDeviceAPI,
  ActiveX, uParseComCmd;

type

  TMyEndpointVolumeCallback = class(TInterfacedObject, IAudioEndpointVolumeCallback)
  public
    function OnNotify(pNotify: PAUDIO_VOLUME_NOTIFICATION_DATA): HRESULT; stdcall;
  end;

  TAutoLvl = (alNone, alNeedDown, alNeedUp);

  TFilter = packed record
//    Time : Integer;
//    Active : Boolean;
    LeftAbsolut : Word;    //�� ����� �������� ����� �����
    LeftCurrent : Word;    //������� ��������
    LeftStep : Single;           //���
    LeftCurStep : Single;
    MaxSteps : Word;
    LeftLvl : Word;
    LeftLvlMax : Word;
    Skiped : Word;           //���������� ������� (���������� ��������)
    CountSkiped : Word;      //���������� �����   (���������� ����������)
    NoSmoothUp : Boolean;    //true - ���� ����� �������� ���� �������� ����� ���������
    procedure Calc(Left : Integer; Right : Integer = -1);
    procedure _Calc;
  end;

  TgForm = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Splitter1: TSplitter;
    ColorDialog1: TColorDialog;
    SpinEditRow: TSpinEdit;
    SpinEditCol: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    SynGeneralSyn1: TSynGeneralSyn;
    SynEdit1: TSynEdit;
    Panel1: TPanel;
    LabelCaretPos: TLabel;
    ImageListGutterGlyphs: TImageList;
    ImageListActions: TImageList;
    ActionListMain: TActionList;
    ActionDebugRun: TAction;
    ActionDebugStep: TAction;
    ActionDebugGotoCursor: TAction;
    ActionDebugPause: TAction;
    ActionDebugStop: TAction;
    ActionToggleBreakpoint: TAction;
    ActionClearAllBreakpoints: TAction;
    ActionCompile: TAction;
    ToolBar1: TToolBar;
    CompileBtn: TToolButton;
    RunBtn: TToolButton;
    StepBtn: TToolButton;
    StopBtn: TToolButton;
    LabelStateCode: TLabel;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    PauseBtn: TToolButton;
    GoToCursorBtn: TToolButton;
    OpenScriptBtn: TToolButton;
    ActionOpenScript: TAction;
    OP: TOpenDialog;
    ToolButton10: TToolButton;
    SD: TSaveDialog;
    ActionSaveScript: TAction;
    Memo1: TMemo;
    BComPort1: TBComPort;
    cbPort: TComboBox;
    ToggleSwitch1: TToggleSwitch;
    TabSheet3: TTabSheet;
    BtnSound: TButton;
    ComboBoxDevice: TComboBox;
    StartVisual: TBitBtn;
    StopVisual: TBitBtn;
    Panel2: TPanel;
    Label3: TLabel;
    ComboBoxFs: TComboBox;
    ComboBoxChannel: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    AnimateComboBox: TComboBox;
    Label6: TLabel;
    Label7: TLabel;
    IconsBtn: TImageList;
    OptionsBtn: TButton;
    Button2: TButton;
    Button1: TButton;
    AutoLvlVolumeBox: TCheckBox;
    Volume: TTrackBar;
    FilterBox: TCheckBox;
    FilterTimeSpinEdit: TSpinEdit;
    FilterVolumeTimer: TTimer;
    Button3: TButton;
    RefreshComPorts: TButton;
    FilterBoxRezkoUp: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpinEditRowChange(Sender: TObject);
    procedure SpinEditColChange(Sender: TObject);
    procedure BitBtnClick(Sender: TObject);
    procedure SynEdit1GutterGetText(Sender: TObject; aLine: Integer; var aText: string);
    procedure SynEdit1StatusChange(Sender: TObject; Changes: TSynStatusChanges);
    procedure FormShow(Sender: TObject);
    procedure SynEdit1GutterClick(Sender: TObject; Button: TMouseButton; X, Y, Line: Integer; Mark: TSynEditMark);
    procedure SynEdit1SpecialLineColors(Sender: TObject; Line: Integer; var Special: Boolean; var FG, BG: TColor);
    procedure ActionDebugRunExecute(Sender: TObject);
    procedure ActionDebugStepExecute(Sender: TObject);
    procedure ActionDebugStopExecute(Sender: TObject);
    procedure ActionDebugGotoCursorExecute(Sender: TObject);
    procedure ActionDebugPauseExecute(Sender: TObject);
    procedure ActionToggleBreakpointExecute(Sender: TObject);
    procedure ActionClearAllBreakpointsExecute(Sender: TObject);
    procedure ActionCompileExecute(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure ActionOpenScriptExecute(Sender: TObject);
    procedure ActionSaveScriptExecute(Sender: TObject);
    procedure ToggleSwitch1Click(Sender: TObject);
    procedure BtnSoundClick(Sender: TObject);
    procedure StartVisualClick(Sender: TObject);
    procedure StopVisualClick(Sender: TObject);
    procedure ComboBoxDeviceSelect(Sender: TObject);
    procedure AnimateComboBoxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure OptionsBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure VolumeChange(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure BComPort1RxChar(Sender: TObject; Count: Integer);
    procedure FilterTimeSpinEditChange(Sender: TObject);
    procedure FilterVolumeTimerTimer(Sender: TObject);
    procedure FilterBoxClick(Sender: TObject);
    procedure RefreshComPortsClick(Sender: TObject);
  private
    { Private declarations }
    FCurrentLine: integer;
    FDebugger: TSampleDebugger;
    ParserScript : TParserScript;
    vsLvlTimeLeft, vsLvlTimeRight : TVisualSoundLevelTime;
    vsLvlLeft, vsLvlRight :  TVisualSoundLevel;
    vsSpectr : TVisualSoundSpectr;
    FAnimEffect : TAnimateEffect;
    FEndpoint: IAudioEndpointVolume;    //
    FVolumeUpdating: Boolean;
    time : Integer;  //for auto lvl volume
    AutoLvlPick : Integer;
    AutoLvlVolume : TAutoLvl;
    procedure DebuggerBreakpointChange(Sender: TObject; ALine: integer);
    procedure DebuggerCurrentLineChange(Sender: TObject);
    procedure DebuggerStateChange(Sender: TObject; OldState, NewState: TDebuggerState);
    procedure DebuggerYield(Sender: TObject);
    procedure PaintGutterGlyphs(ACanvas: TCanvas; AClip: TRect; FirstLine, LastLine: integer);
    procedure SetCurrentLine(ALine: integer);
    procedure FillBitBtn(Btn : TBitBtn; cl : TColor);
    procedure ShowLogStep;
    procedure SoundLvlNotify(Sender: TObject; lvl_Left, lvl_Right : Integer);
    procedure InitVolume;
    procedure SetVolumeControl;
    procedure UpdateVolume;
    procedure OnLedTowerSizeEvent(Sender: TObject; Row, Col : Integer);
    procedure SendHeadLvlTowerAssign;
  public
    { Public declarations }
    Filter : TFilter;
    SoundSpectrum : TSoundSpectrum;
    LedDisplay : TLedDisplay;
    procedure OnWaveIn(var Msg: TMessage); message MM_WIM_DATA;
    procedure SendStateLedTower(Sender: TObject);
    property VolumeUpdating: Boolean read FVolumeUpdating write FVolumeUpdating;
  end;

  TComboIndexDevice = class(TObject)
  public
    Index : Integer;
  end;

var
  gForm: TgForm;

implementation

//uses
//  Vcl.Styles.Utils.SysControls, Vcl.FileCtrl;

{$R *.dfm}

type
  TDebugSupportPlugin = class(TSynEditPlugin)
  protected
    fForm: TgForm;
    procedure AfterPaint(ACanvas: TCanvas; const AClip: TRect; FirstLine, LastLine: integer); override;
    procedure LinesInserted(FirstLine, Count: integer); override;
    procedure LinesDeleted(FirstLine, Count: integer); override;
  public
    constructor Create(AForm: TgForm);
  end;

constructor TDebugSupportPlugin.Create(AForm: TgForm);
begin
  inherited Create(AForm.SynEdit1);
  fForm := AForm;
end;

procedure TDebugSupportPlugin.AfterPaint(ACanvas: TCanvas; const AClip: TRect; FirstLine, LastLine: integer);
begin
  fForm.PaintGutterGlyphs(ACanvas, AClip, FirstLine, LastLine);
end;

procedure TDebugSupportPlugin.LinesInserted(FirstLine, Count: integer);
begin
// Note: You will need this event if you want to track the changes to
//       breakpoints in "Real World" apps, where the editor is not read-only
end;

procedure TDebugSupportPlugin.LinesDeleted(FirstLine, Count: integer);
begin
// Note: You will need this event if you want to track the changes to
//       breakpoints in "Real World" apps, where the editor is not read-only
end;

procedure DivideEllipse(canvas :TCanvas; const rc: TRect; const colors : array of TColor; const iParts : integer);
var
  x3, y3, x4, y4 : Integer;
  x0, y0 : Double; 
  a, b, anglePart : Double;
  iColorIndex, iColorCount, i : Integer;
const              
  dToRad = PI / 180.0;
begin
  x0 := (rc.left + rc.right ) / 2;
  y0 := ( rc.top + rc.bottom) / 2;
  a  := (rc.right - rc.left ) / 2;
  b  := (rc.bottom - rc.top ) / 2;
  anglePart := 360.0 / iParts;

  iColorIndex := 0;
  iColorCount := Length(colors)-1;

  for i := 0 to iParts do
  begin
    if iColorIndex >= iColorCount then iColorIndex := 0 else Inc(iColorIndex);
    x4 := Round(x0 + a * cos( ( i * anglePart ) * dToRad ));
    y4 := Round(y0 + b * sin( ( i * anglePart ) * dToRad ));

    x3 := Round(x0 + a * cos( ( i * anglePart + anglePart ) * dToRad ));
    y3 := Round(y0 + b * sin( ( i * anglePart + anglePart ) * dToRad ));

    canvas.Pen.Color := colors[ iColorIndex ];
    canvas.Brush.Color := colors[ iColorIndex ];
    canvas.Pie( rc.left, rc.top, rc.right, rc.bottom, x3, y3, x4, y4 );
  end;
// }
end;

procedure circle(canvas:tcanvas;x,y,r:integer);
begin           //
canvas.Ellipse(x,y,x+r,y+r);
end;

procedure TgForm.FillBitBtn(Btn : TBitBtn; cl : TColor);
begin
  Btn.Glyph.SetSize(Btn.Height-8, Btn.Height-8);
  Btn.Glyph.Canvas.Brush.Color := cl;
  Btn.Glyph.Canvas.Rectangle(0, 0, Btn.Glyph.Width, Btn.Glyph.Height);
end;

procedure TgForm.FilterBoxClick(Sender: TObject);
begin
  FilterVolumeTimer.Enabled := FilterBox.Checked;
end;

procedure TgForm.FilterTimeSpinEditChange(Sender: TObject);
begin
  Filter.MaxSteps := FilterTimeSpinEdit.Value div 15;
end;

procedure TgForm.FilterVolumeTimerTimer(Sender: TObject);
begin
  if StartVisual.Visible then exit;

  Inc(Filter.Skiped);
  if Filter.Skiped < Filter.CountSkiped then
  begin
    Filter.LeftCurrent := Round(Filter.LeftCurrent + Filter.LeftStep);
  end
  else
  if Filter.Skiped > Filter.CountSkiped then
  begin
    Filter._Calc;
    Filter.Skiped := 1;
    Filter.LeftLvlMax := 0;
  end
  else
  if Filter.Skiped = Filter.CountSkiped then Filter.LeftCurrent := Filter.LeftAbsolut;

  FAnimEffect.Animate(Filter.LeftCurrent, Filter.LeftCurrent);
end;

procedure TgForm.ActionClearAllBreakpointsExecute(Sender: TObject);
begin
  FDebugger.ClearAllBreakpoints;
end;

procedure TgForm.ActionCompileExecute(Sender: TObject);
begin
//  SynEdit1.Lines
  ParserScript.ParseExecutableLines;
  SynEdit1.Refresh;
end;

procedure TgForm.ActionDebugGotoCursorExecute(Sender: TObject);
begin
  FDebugger.GotoCursor(SynEdit1.CaretY);
end;

procedure TgForm.ActionDebugPauseExecute(Sender: TObject);
begin
  FDebugger.Pause;
  RunBtn.Enabled := true;
end;

procedure TgForm.ActionDebugRunExecute(Sender: TObject);
begin
  SynEdit1.ReadOnly := true;
  StopBtn.Enabled := true;
  CompileBtn.Enabled := true;

  ParserScript.ParseExecutableLines;
  ParserScript.ParseSampleCode;
  if ParserScript.Error then begin ActionDebugStopExecute(ActionDebugStop); exit; end;

  FDebugger.Run;
  ParserScript.Sleep := 500;
end;

procedure TgForm.ActionDebugStepExecute(Sender: TObject);
begin
  if not StopBtn.Enabled then
  begin
    ParserScript.ParseExecutableLines;
    ParserScript.ParseSampleCode;
    if ParserScript.Error then begin ActionDebugStopExecute(ActionDebugStop); exit; end;

    SynEdit1.ReadOnly := true;
    RunBtn.Enabled := true;
    StopBtn.Enabled := true;
    CompileBtn.Enabled := false;
    FDebugger.Step;
    ShowLogStep;
  end
  else
  begin
    FDebugger.Step;
    ShowLogStep;
    LedDisplay.PaintOnlyLed;
    SendStateLedTower(nil);
  end;
end;

procedure TgForm.ActionDebugStopExecute(Sender: TObject);
begin
  CompileBtn.Enabled := true;
  SynEdit1.ReadOnly := false;
  StopBtn.Enabled := false;

  FDebugger.Stop;
  LedDisplay.LED_off;
end;

procedure TgForm.ActionOpenScriptExecute(Sender: TObject);
begin
//  if OP.Execute then
  try
//    SynEdit1.Lines.LoadFromFile(OP.FileName);
    SynEdit1.Lines.LoadFromFile('C:\Script\Test.lts');
    FDebugger.ExecutableLines.Clear;
    ActionCompileExecute(Self);
  except
    ShowMessage('Can''t open file!');
  end;
end;

procedure TgForm.ActionSaveScriptExecute(Sender: TObject);
begin
  if SD.Execute then
  try
    SynEdit1.Lines.SaveToFile(SD.FileName);
    ActionCompileExecute(Self);
  except
    ShowMessage('Can''t save file!');
  end;
end;

procedure TgForm.ActionToggleBreakpointExecute(Sender: TObject);
begin
  FDebugger.ToggleBreakpoint(SynEdit1.CaretY);
end;

procedure TgForm.AnimateComboBoxDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  r : TRect;
  str, str2, str3 : String;
  k : SmallInt;
begin
  with AnimateComboBox.Canvas do
  begin
    r := Rect;
    str := AnimateComboBox.Items.Strings[Index];
    str2 := ComboBoxChannel.Items.Strings[ TAnimateEffect(AnimateComboBox.Items.Objects[Index]).Channel-1 ];
    str3 := TAnimateEffect(AnimateComboBox.Items.Objects[Index]).CaptionOptions;

    if odSelected in State then
    begin
      Brush.Color := TStyleManager.ActiveStyle.GetSystemColor(clHotLight);
      FillRect(Rect);

      Inc(r.Left, 2);
      TextRect(r, str, []);

      Dec(r.Right, 2);
      Font.Color := clYellow;
      TextRect(r, str2, [tfRight, tfVerticalCenter]);

      Inc(r.Left, TextWidth(str) + 4);
      if TAnimateEffect(AnimateComboBox.Items.Objects[Index]).options then
      begin
        k := TextWidth(str);
        IconsBtn.Draw(AnimateComboBox.Canvas, k, Rect.Top-3, 1, true);
      end;

      Inc(r.Left, IconsBtn.Width-6);
      Font.Color := clWhite;
      TextRect(r, str3, []);
    end
    else
    begin
      Brush.Color := TStyleManager.ActiveStyle.GetSystemColor(clWindow);
      FillRect(Rect);

      Inc(r.Left, 2);
      TextRect(r, str, []);

      Dec(r.Right, 2);
      Font.Color := clGreen;
      TextRect(r, str2, [tfRight, tfVerticalCenter]);

      Inc(r.Left, TextWidth(str) + 4);
      if TAnimateEffect(AnimateComboBox.Items.Objects[Index]).options then
      begin
        k := TextWidth(str);
        IconsBtn.Draw(AnimateComboBox.Canvas, k, Rect.Top-3, 1, true);
      end;

      Inc(r.Left, IconsBtn.Width-6);
      Font.Color := clWhite;
      TextRect(r, str3, []);
    end;
  end;

end;

procedure TgForm.StartVisualClick(Sender: TObject);
const
  Fs : array[0..5] of DWord = (11025, 22050, 44100, 48000, 96000, 192000);
  Channel : array[0..1] of Byte = (1, 2);
begin
  StartVisual.Visible := false;
  StopVisual.Visible := true;

  SoundSpectrum.SpectrGercel := true;
  SoundSpectrum.SoundLvl := true;

  SoundSpectrum.OnSoundLvlNotify := nil;
  SoundSpectrum.OnSoundLvlNotify := SoundLvlNotify;
  FAnimEffect.PrepareToAnim;

  SoundSpectrum.Start(Handle, ComboBoxDevice.ItemIndex, Fs[ComboBoxFs.ItemIndex], FAnimEffect.Channel);
  time := GetTickCount;
  AutoLvlVolume := alNone;
//filter
  Filter.Skiped := 2;
  Filter.CountSkiped := 1;
  Filter.LeftCurrent := 0;
//
  SendHeadLvlTowerAssign;
end;

procedure TgForm.StopVisualClick(Sender: TObject);
begin
  StopVisual.Visible := false;
  StartVisual.Visible := true;
  SoundSpectrum.Stop;

  SoundSpectrum.SoundLvlRight.Position := 0;
  SoundSpectrum.SoundLvlTimeRight.Clear;
end;

procedure TgForm.BComPort1RxChar(Sender: TObject; Count: Integer);
var
  buf : TByteArray;
  i : integer;
//  s : String;
begin
  if Count < 3 then exit;
  BComPort1.Read(buf[0], Count);
//  s := '';
//  for i := 0 to Count-1 do s := s + ' ' + buf[i].ToString;
//  Memo1.Lines.Add('Count ' + Count.ToString + ' ->' + s);

  cmd.Parse(@buf, Count);
end;

procedure TgForm.BitBtnClick(Sender: TObject);
begin
  if ColorDialog1.Execute then
  begin
    FillBitBtn((Sender as TBitBtn), ColorDialog1.Color);
    case (Sender as TBitBtn).Tag of
      51: LedDisplay.ColorLedBrushOff    := BitBtn1.Glyph.Canvas.Pixels[1, 1];
      52: LedDisplay.ColorLedPenOff      := BitBtn2.Glyph.Canvas.Pixels[1, 1];
      53: LedDisplay.ColorLedBrushActive := BitBtn3.Glyph.Canvas.Pixels[1, 1];
      54: LedDisplay.ColorLedPenActive   := BitBtn4.Glyph.Canvas.Pixels[1, 1];
      55: LedDisplay.ColorPie            := BitBtn5.Glyph.Canvas.Pixels[1, 1];
      56: LedDisplay.FontNumberColor     := BitBtn6.Glyph.Canvas.Pixels[1, 1];
    end;
    LedDisplay.Refresh;
  end;
end;

procedure TgForm.BtnSoundClick(Sender: TObject);
  function GetWaveInDevice: Cardinal;   // this method will return the index in the list
  const
    DRVM_MAPPER=$2000;
    DRVM_MAPPER_PREFERRED_GET = DRVM_MAPPER + 21;
    DRVM_MAPPER_PREFERRED_SET = DRVM_MAPPER + 22;
  var
   LDW2: Cardinal;
  begin
   Result := $FFFFFFFF;
   LDW2 := 0;
   waveInMessage( WAVE_MAPPER, DRVM_MAPPER_PREFERRED_GET, DWORD( @Result ), DWORD( @LDW2 ) );
  end;

var
  i, k: integer;
  w:    TWAVEINCAPS;
  n:    string;
begin
  for i := 0 to ComboBoxDevice.Items.Count-1 do ComboBoxDevice.Items.Objects[i].free;
  ComboBoxDevice.Items.Clear;

  k := waveInGetNumDevs; // Device Count
  for i := 0 to k - 1 do
  begin
    FillChar(w, SizeOf(TWAVEINCAPS), #0);
    WaveInGetDevCaps(i, @w, SizeOf(TWAVEINCAPS)); // GetDeviceInfo
    n := w.szPname; // DeviceName
//    ComboBoxDevice.Items.AddObject(n, pointer(w.wMid));
    ComboBoxDevice.Items.Add(n);
  end;

  InitVolume;
  ComboBoxDevice.ItemIndex := GetWaveInDevice; //�� ���������
  SetVolumeControl;
end;

procedure TgForm.Button1Click(Sender: TObject);
begin
  gForm.SoundSpectrum.SetBitPerSampleOnlyTest(16);
  FAnimEffect.PrepareToAnim;
  FAnimEffect.Animate(4000);
//  FAnimEffect.Animate(16385);
  if BComPort1.Connected then SendStateLedTower(self);
end;

procedure TgForm.Button2Click(Sender: TObject);
begin
//  SoundLvlNotify(nil, 32767, 32767);
end;

procedure TgForm.Button3Click(Sender: TObject);
var
  m : TByteArray;
  c : Word;
begin
  if BComPort1.Connected then
  begin
    cmd.PrepareToSendSetHead(FilterTimeSpinEdit.Value div 10, m, c);
    BComPort1.Write(m, c);
  end;
//  SoundLvlNotify(nil, 16384, 16384);

end;

procedure TgForm.RefreshComPortsClick(Sender: TObject);
begin
  EnumComPorts(cbPort.Items);
  cbPort.ItemIndex := 0;
  if cbPort.Items.Count = 2 then cbPort.ItemIndex := 1;
end;

procedure TgForm.ComboBoxDeviceSelect(Sender: TObject);
begin
  if StartVisual.Visible = false then
  begin
    StopVisual.Click;

    FAnimEffect := TAnimateEffect(AnimateComboBox.Items.Objects[AnimateComboBox.ItemIndex]);
    ComboBoxChannel.ItemIndex := FAnimEffect.Channel-1;

    StartVisual.Click;
  end
  else
  begin
    FAnimEffect := TAnimateEffect(AnimateComboBox.Items.Objects[AnimateComboBox.ItemIndex]);
    ComboBoxChannel.ItemIndex := FAnimEffect.Channel-1;
  end;

  OptionsBtn.Visible := FAnimEffect.options;
  SetVolumeControl
end;

procedure TgForm.DebuggerBreakpointChange(Sender: TObject; ALine: integer);
begin
  if (ALine >= 1) and (ALine <= SynEdit1.Lines.Count) then
  begin
    SynEdit1.InvalidateGutterLine(ALine);
    SynEdit1.InvalidateLine(ALine);
  end
  else
    SynEdit1.Invalidate;
end;

procedure TgForm.DebuggerCurrentLineChange(Sender: TObject);
begin
  if (FDebugger <> nil) and not FDebugger.IsRunning then SetCurrentLine(FDebugger.CurrentLine)
                                                    else SetCurrentLine(-1);
end;

procedure TgForm.DebuggerStateChange(Sender: TObject; OldState, NewState: TDebuggerState);
var
  s: string;
begin
  case NewState of
    dsRunning: s := 'Program is running';
    dsPaused: s := 'Program is paused';
  else
    s := 'Ready';
  end;
  LabelStateCode.Caption := ' ' + s;
end;

procedure TgForm.DebuggerYield(Sender: TObject);
begin
  UpdateActions;
  Application.ProcessMessages;
end;

procedure TgForm.FormCreate(Sender: TObject);
var
  Settings: TStringList;
  AnimEff : TAnimateEffect;
  AnimEffSimple_M : TAnimEffectSimple_Mono;
  AnimEffBorder : TAnimEffectOnlyBorder_Mono;
  AnimEffStairs : TAnimEffectStairs_Mono;
  AnimEffSimple_S : TAnimEffectSimple_Stereo;
  AnimEffectOnlyBorderShift : TAnimEffectOnlyBorderShift_Mono;
  AnimEffectScanShift_Mono : TAnimEffectScanShift_Mono;
begin
  LedDisplay := TLedDisplay.Create(Self);
  LedDisplay.Parent := Self;
  LedDisplay.Left := Self.ClientWidth - LedDisplay.Width;
  LedDisplay.Top := 0;
  LedDisplay.Height := Self.ClientHeight;
  LedDisplay.Anchors := [akTop, akBottom, akRight];
  LedDisplay.Align := alRight;
  LedDisplay.Calc;

  Splitter1.Align := alRight;
  Splitter1.Left := PageControl1.Width+5;

  FillBitBtn(BitBtn1, LedDisplay.ColorLedBrushOff);
  FillBitBtn(BitBtn2, LedDisplay.ColorLedPenOff);

  FillBitBtn(BitBtn3, LedDisplay.ColorLedBrushActive);
  FillBitBtn(BitBtn4, LedDisplay.ColorLedPenActive);

  FillBitBtn(BitBtn5, LedDisplay.ColorPie);

  FillBitBtn(BitBtn6, LedDisplay.FontNumberColor);


  FCurrentLine := -1;
  FDebugger := TSampleDebugger.Create;
  with FDebugger do begin
    OnBreakpointChange := DebuggerBreakpointChange;
    OnCurrentLineChange := DebuggerCurrentLineChange;
    OnStateChange := DebuggerStateChange;
    OnYield := DebuggerYield;
    OnSendStateLedTower := SendStateLedTower;
  end;

  TDebugSupportPlugin.Create(Self);

  Settings := TStringList.Create;
  try
    SynGeneralSyn1.EnumUserSettings(Settings);
    if Settings.Count > 0 then SynGeneralSyn1.UseUserSettings(Settings.Count - 1);
  finally
    Settings.Free;
  end;
  ParserScript := TParserScript.Create(LedDisplay, SynEdit1.Lines, FDebugger.ExecutableLines, FDebugger.SampleCode);
  ParserScript.hwndForMsgBox := Self.Handle;
  FDebugger.Parser := ParserScript;

//��� ������������ �����
  OpenScriptBtn.Click;  /////////only for debug -------------------

  BtnSoundClick(BtnSound);
  SoundSpectrum := TSoundSpectrum.Create;
  SoundSpectrum.OnSoundLvlNotify := SoundLvlNotify;

  vsLvlTimeRight := TVisualSoundLevelTime.Create(TabSheet3);
  vsLvlTimeRight.Parent := TabSheet3;
  vsLvlTimeRight.SetBounds(30,
    TabSheet3.Height - vsLvlTimeRight.Height,
    TabSheet3.Width - vsLvlTimeRight.Left,
    vsLvlTimeRight.Height
    );
  vsLvlTimeRight.Anchors := [akLeft, akRight, akBottom];

  vsLvlTimeLeft := TVisualSoundLevelTime.Create(TabSheet3);
  vsLvlTimeLeft.Parent := TabSheet3;
  vsLvlTimeLeft.SetBounds(30,
    TabSheet3.Height - vsLvlTimeLeft.Height - vsLvlTimeRight.Height - 3,
    TabSheet3.Width - vsLvlTimeLeft.Left,
    vsLvlTimeLeft.Height
    );
  vsLvlTimeLeft.Anchors := [akLeft, akRight, akBottom];

  vsLvlRight := TVisualSoundLevel.Create(TabSheet3);
  vsLvlRight.Parent := TabSheet3;
  vsLvlRight.SetBounds(3, vsLvlTimeRight.Top+3, vsLvlRight.Width, vsLvlTimeRight.Height-3);
  vsLvlRight.Anchors := [akLeft, akBottom];

  vsLvlLeft := TVisualSoundLevel.Create(TabSheet3);
  vsLvlLeft.Parent := TabSheet3;
  vsLvlLeft.SetBounds(3, vsLvlTimeLeft.Top+3, vsLvlLeft.Width, vsLvlTimeLeft.Height-3);
  vsLvlLeft.Anchors := [akLeft, akBottom];

  vsSpectr := TVisualSoundSpectr.Create(TabSheet3);
  vsSpectr.Parent := TabSheet3;
  vsSpectr.SetBounds(3, vsLvlLeft.Top-175, TabSheet3.Width-3, 170);
  vsSpectr.Mode := vssmLine;
  vsSpectr.FixedHeight := true;
  vsSpectr.MaxHeightColumn := 4;
  vsSpectr.Anchors := [akLeft, akBottom, akRight];

  SoundSpectrum.SoundLvlLeft      := vsLvlLeft;
  SoundSpectrum.SoundLvlRight     := vsLvlRight;
  SoundSpectrum.SoundLvlTimeLeft  := vsLvlTimeLeft;
  SoundSpectrum.SoundLvlTimeRight := vsLvlTimeRight;
  SoundSpectrum.SoundSpectr       := vsSpectr;

  AnimEffSimple_M := TAnimEffectSimple_Mono.Create;
  AnimEffSimple_M.Channel := 1;
  AnimEffSimple_M.Filter := 5;
  AnimEffSimple_M.animShift := asUp;
  AnimEffSimple_M.options := true;
  AnimEffSimple_M.ReName;
  AnimEffSimple_M.itemIndexCaption := AnimateComboBox.Items.AddObject(AnimEffSimple_M.Caption, AnimEffSimple_M);

  AnimEffBorder := TAnimEffectOnlyBorder_Mono.Create;
  AnimEffBorder.Channel := 1;
  AnimEffBorder.Filter := 5;
  AnimEffBorder.animShift := obUp;
  AnimEffBorder.options := true;
  AnimEffBorder.ReName;
  AnimEffBorder.itemIndexCaption := AnimateComboBox.Items.AddObject(AnimEffBorder.Caption, AnimEffBorder);

  AnimEffStairs := TAnimEffectStairs_Mono.Create;
  AnimEffStairs.Channel := 1;
  AnimEffStairs.Filter := 5;
  AnimEffStairs.AnimStairs := asMount;
  AnimEffStairs.options := true;
  AnimEffStairs.ReName;
  AnimEffStairs.itemIndexCaption := AnimateComboBox.Items.AddObject(AnimEffStairs.Caption, AnimEffStairs);

  AnimEffectOnlyBorderShift := TAnimEffectOnlyBorderShift_Mono.Create;
  AnimEffectOnlyBorderShift.Channel := 1;
  AnimEffectOnlyBorderShift.Filter := 5;
  AnimEffectOnlyBorderShift.options := false;
  AnimEffectOnlyBorderShift.ReName;
  AnimEffectOnlyBorderShift.itemIndexCaption := AnimateComboBox.Items.AddObject(AnimEffectOnlyBorderShift.Caption, AnimEffectOnlyBorderShift);

  AnimEffSimple_S := TAnimEffectSimple_Stereo.Create;
  AnimEffSimple_S.Channel := 2;
  AnimEffSimple_S.Filter := 5;
  AnimEffSimple_S.AnimSimple := assCenter;
  AnimEffSimple_S.options := true;
  AnimEffSimple_S.ReName;
  AnimEffSimple_S.itemIndexCaption := AnimateComboBox.Items.AddObject(AnimEffSimple_S.Caption, AnimEffSimple_S);

  AnimEffectScanShift_Mono := TAnimEffectScanShift_Mono.Create;
  AnimEffectScanShift_Mono.Channel := 1;
  AnimEffectScanShift_Mono.Filter := 5;
  AnimEffectScanShift_Mono.animShift := asRight;
  AnimEffectScanShift_Mono.AnimScanShift := assDots;
  AnimEffectScanShift_Mono.putDown := true;
  AnimEffectScanShift_Mono.options := true;
  AnimEffectScanShift_Mono.ReName;
  AnimEffectScanShift_Mono.itemIndexCaption := AnimateComboBox.Items.AddObject(AnimEffectScanShift_Mono.Caption, AnimEffectScanShift_Mono);

//  AnimateComboBox.ItemIndex := AnimateComboBox.Items.Count-1;
  AnimateComboBox.ItemIndex := 0;
  FAnimEffect := TAnimateEffect(AnimateComboBox.Items.Objects[AnimateComboBox.ItemIndex]);

  cmd.FOnLedTowerSizeEvent := OnLedTowerSizeEvent;

  RefreshComPorts.Click;
end;

procedure TgForm.FormDestroy(Sender: TObject);
var
  i : Integer;
begin
  if FDebugger.IsRunning then FDebugger.Stop;
  FDebugger.Free;
  FreeAndNil(ParserScript);
  FreeAndNil(LedDisplay);
  FreeAndNil(vsLvlTimeLeft);
  FreeAndNil(vsLvlTimeRight);
  FreeAndNil(SoundSpectrum);
  FreeAndNil(vsSpectr);

  for i := 0 to ComboBoxDevice.Items.Count-1 do ComboBoxDevice.Items.Objects[i].free;
end;

procedure TgForm.FormShow(Sender: TObject);
begin
//  SynEdit1.SetFocus;
end;

procedure TgForm.InitVolume;
  function findStringVolumeInComboBox(Name : String): Integer;
  var
    i, c : Integer;
  begin
    Result := -1;
    c := ComboBoxDevice.Items.Count-1;
    for i := 0 to c do
      if Pos(ComboBoxDevice.Items[i], Name) > 0 then
      begin
        Result := i;
        break;
      end;
  end;

const
  fmtidn : TGUID = '{A45C254E-DF1C-4EFD-8020-67D146A850E0}';
  fmtid : TGUID = '{F19F064D-082C-4E27-BC73-6882A1BB8E4C}';
var
  PKEY_Device_FriendlyName: _tagpropertykey;
 DNum: integer;
 i, index: integer;
// Caps: TWaveOutCaps;

 pEnumerator: IMMDeviceEnumerator;
 pCollection: IMMDeviceCollection;
 pEndPoint: IMMDevice;
 hr: HRESULT;
 mask: DWORD;
 count: Cardinal;
 friendlyName: string;
 pProps: IPropertyStore;
 varName: PROPVARIANT;
 PropVar :PUserType2;
 FormatWave : TWaveFormatEx;
 ComInd : TComboIndexDevice;
begin
  PKEY_Device_FriendlyName.fmtid := fmtidn;
  PKEY_Device_FriendlyName.pid := 14;

  HR := CoCreateInstance(CLASS_MMDeviceEnumerator, nil, CLSCTX_INPROC_SERVER, IID_IMMDeviceEnumerator, pEnumerator);

  // get the endpoint collection
  mask := 1;
  hr := pEnumerator.EnumAudioEndpoints(eCapture, 1, pCollection);

  // get the size of the collection
  count := 0;
  hr := pCollection.GetCount(count);

  for i := 0 to (count - 1) do
  begin
    // get the endpoint
    hr := pCollection.Item(i, pEndPoint);

    // get the human readable name
    hr := pEndPoint.OpenPropertyStore(STGM_READ, pProps);
    PropVariantInit(varName);
    hr := pProps.GetValue(PKEY_Device_FriendlyName, varName);
    friendlyName := string(varName.pwszVal);
    PropVariantClear(varName);


    index := findStringVolumeInComboBox(friendlyName);
    if index > -1 then
    begin
      ComInd := TComboIndexDevice.Create;
      ComInd.Index := i;
      ComboBoxDevice.Items[index] := friendlyName;
      ComboBoxDevice.Items.Objects[index] := ComInd;
    end;
//    ShowMessage(friendlyName);
  end;

//  PKEY_Device_FriendlyName.fmtid := fmtid;
//  PKEY_Device_FriendlyName.pid := 0;
//  PropVariantInit(varName);
//  hr := pProps.GetValue(PKEY_Device_FriendlyName, varName);
//  Move(varName.blob.pBlobData^, FormatWave, SizeOf(FormatWave));
//
//  ShowMessage(FormatWave.nChannels.ToString + ' ' + FormatWave.nSamplesPerSec.ToString + ' ' + FormatWave.wBitsPerSample.ToString);
//  pEndPoint.Activate(IID_IAudioEndpointVolume, CLSCTX_ALL, PropVar^, Pointer(FEndPoint));
//  PropVariantClear(varName);
end;

procedure TgForm.SoundLvlNotify(Sender: TObject; lvl_Left, lvl_Right: Integer);
  function IsPick(lvlLeft, lvlRight: Integer):Boolean;
  var
    d : integer;
  begin
    Result := false;
    if gForm.SoundSpectrum.BitPerSample = 16 then d := 32767
      else if gForm.SoundSpectrum.BitPerSample = 8 then d := 128;

    lvlLeft   := ConToReal(lvlLeft,   0, d, 0, 100);
    lvlRight := ConToReal(lvl_Right, 0, d, 0, 100);

    if (lvlLeft > 90)or(lvlRight > 90) then Result := true;
  end;

const LvlStepUp   = 11;
      LvlStepDown = 4;
      Step = 1;
var
  _t : Integer;
  OverPick : Boolean;
begin
//================AutoLvl===========================================================
  if AutoLvlVolumeBox.Checked then
  begin
    // ~20 calls per second
    OverPick := IsPick(lvl_Left, lvl_Right);
    if OverPick then Inc(AutoLvlPick);  //count pick

    _t := GetTickCount;
    if (_t - time) > 1000 then
    begin
      time := _t;

      if AutoLvlPick >= LvlStepUp then AutoLvlVolume := alNeedDown
        else if AutoLvlPick <= LvlStepDown then AutoLvlVolume := alNeedUp;

//      gForm.Caption := AutoLvlPick.ToString;
      AutoLvlPick := 0;
    end;

    if AutoLvlVolume = alNeedDown then
    begin
      if Volume.Position - Step >= 0 then Volume.Position := Volume.Position - Step;
      if not OverPick then AutoLvlVolume := alNone;
    end
    else
    if AutoLvlVolume = alNeedUp then
    begin
      if Volume.Position + Step <= 100 then Volume.Position := Volume.Position + Step;
      if OverPick then AutoLvlVolume := alNone;
    end;
  end;
//================Filter============================================================
//  if FilterBox.Checked then
//  begin
//    Inc(Filter.Skiped);
//    if Filter.Skiped < Filter.CountSkiped then
//    begin
//      Filter.LeftCurrent := Round(Filter.LeftCurrent + Filter.LeftStep);
//    end
//    else
//    if Filter.Skiped > Filter.CountSkiped then
//    begin
//      Filter.Calc(lvl_Left);
//      Filter.Skiped := 1;
//    end
//    else
//    if Filter.Skiped = Filter.CountSkiped then Filter.LeftCurrent := Filter.LeftAbsolut;
//
//    FAnimEffect.Animate(Filter.LeftCurrent, Filter.LeftCurrent);
//    exit;
//  end;
  if FilterBox.Checked then
  begin
    if Filter.LeftLvlMax < lvl_Left then Filter.LeftLvlMax := lvl_Left;
    Filter.LeftLvl := lvl_Left;
    if FilterVolumeTimer.Enabled = false then Filter._Calc;
    exit;
  end;
//==================================================================================
  FAnimEffect.Animate(lvl_Left, lvl_Right);
end;

procedure TgForm.OnLedTowerSizeEvent(Sender: TObject; Row, Col: Integer);
begin
  SpinEditRow.Value := Row;
  SpinEditCol.Value := Col;
end;

procedure TgForm.OnWaveIn(var Msg: TMessage);
begin
  SoundSpectrum.OnWaveIn(Msg);
end;

procedure TgForm.OptionsBtnClick(Sender: TObject);
begin
  FAnimEffect.ShowOptions;
end;

procedure TgForm.SendHeadLvlTowerAssign;
var
  m : TByteArray;
  c : Word;
begin
  if BComPort1.Connected then
  begin
    cmd.PrepareToSendSetHead(FAnimEffect.defaultHead, m, c);
    BComPort1.Write(m, c);
  end;
end;

procedure TgForm.SendStateLedTower(Sender: TObject);
var
  buf : array [1..35] of byte;
  i, j, r, c: word;
  ResByte, tmpByte, ostatok, num : byte;
begin
  if BComPort1.Connected = true then
  begin
    buf[1] := SendByte1;
    buf[2] := SendByte2;
    buf[3] := cmdComLedState;

    num := 4;
    c := LedDisplay.Cols-1;
    r := LedDisplay.Rows-1;
    for i := 0 to r do
    begin
      ResByte := 0;
      for j := 0 to c do
      begin
        ostatok := j mod 8;
        if ostatok = 0 then ResByte := 0;
        tmpByte := IfThen(LedDisplay.Leds[i, j].Vis, 1, 0);
//        tmpByte := tmpByte shl (7 - ostatok);  //���������� �����������
        tmpByte := tmpByte shl ostatok;          //�������� ������� ���
        ResByte := ResByte or tmpByte;
        if (ostatok = 7)or(j = r) then
        begin
          buf[num] := ResByte;
          Inc(num);
        end;
      end;
    end;

    BComPort1.Write(buf, 35);
  end;
end;

procedure TgForm.PaintGutterGlyphs(ACanvas: TCanvas; AClip: TRect; FirstLine, LastLine: integer);
var
  LH, X, Y: integer;
  LI: TDebuggerLineInfos;
  ImgIndex: integer;
begin
  if FDebugger <> nil then
  begin
    FirstLine := SynEdit1.RowToLine(FirstLine);
    LastLine := SynEdit1.RowToLine(LastLine);
    X := 14;
    LH := SynEdit1.LineHeight;
    while FirstLine <= LastLine do
    begin
      Y := (LH - ImageListGutterGlyphs.Height) div 2 + LH * (SynEdit1.LineToRow(FirstLine) - SynEdit1.TopLine);
      LI := FDebugger.GetLineInfos(FirstLine);
      if dlCurrentLine in LI then begin
        if dlBreakpointLine in LI then
          ImgIndex := 2
        else
          ImgIndex := 1;
      end else if dlExecutableLine in LI then begin
        if dlBreakpointLine in LI then
          ImgIndex := 3
        else
          ImgIndex := 0;
      end else begin
        if dlBreakpointLine in LI then
          ImgIndex := 4
        else
          ImgIndex := -1;
      end;
      if ImgIndex >= 0 then
        ImageListGutterGlyphs.Draw(ACanvas, X, Y, ImgIndex);
      Inc(FirstLine);
    end;
  end;
end;

procedure TgForm.SetCurrentLine(ALine: integer);
begin
  if FCurrentLine <> ALine then
  begin
    SynEdit1.InvalidateGutterLine(FCurrentLine);
    SynEdit1.InvalidateLine(FCurrentLine);
    FCurrentLine := ALine;
    if (FCurrentLine > 0) and (SynEdit1.CaretY <> FCurrentLine) then
      SynEdit1.CaretXY := BufferCoord(1, FCurrentLine);
    SynEdit1.InvalidateGutterLine(FCurrentLine);
    SynEdit1.InvalidateLine(FCurrentLine);
  end;
end;

procedure TgForm.SetVolumeControl;
const
  fmtidn : TGUID = '{A45C254E-DF1C-4EFD-8020-67D146A850E0}';
var
  PKEY_Device_FriendlyName: _tagpropertykey;
// Caps: TWaveOutCaps;
  pEnumerator: IMMDeviceEnumerator;
  pCollection: IMMDeviceCollection;
  pEndPoint: IMMDevice;
  hr: HRESULT;
  count: Cardinal;
  friendlyName: string;
  pProps: IPropertyStore;
  varName: PROPVARIANT;
//  varName :PUserType2;
  MyEndpointVolumeCallback: IAudioEndpointVolumeCallback;
  lvl : Double;
  l : Double;
begin
  PKEY_Device_FriendlyName.fmtid := fmtidn;
  PKEY_Device_FriendlyName.pid := 14;

  HR := CoCreateInstance(CLASS_MMDeviceEnumerator, nil, CLSCTX_INPROC_SERVER, IID_IMMDeviceEnumerator, pEnumerator);

  // get the endpoint collection
  hr := pEnumerator.EnumAudioEndpoints(eCapture, 1, pCollection);

  // get the endpoint
  hr := pCollection.Item(TComboIndexDevice(ComboBoxDevice.Items.Objects[ComboBoxDevice.ItemIndex]).Index, pEndPoint);

  // get the human readable name
//  hr := pEndPoint.OpenPropertyStore(STGM_READ, pProps);
//  PropVariantInit(varName);
//  hr := pProps.GetValue(PKEY_Device_FriendlyName, varName);
//  friendlyName := string(varName.pwszVal);
//  PropVariantClear(varName);

//  ShowMessage(friendlyName);

  pEndPoint.Activate(IID_IAudioEndpointVolume, CLSCTX_ALL, varName, Pointer(FEndPoint));
  // Volume changes handler.
  MyEndpointVolumeCallback := TMyEndpointVolumeCallback.Create;
  FEndpoint.RegisterControlChangeNotify(MyEndpointVolumeCallback);

  UpdateVolume;
end;

procedure TgForm.ShowLogStep;
  function ToStr(b : Boolean):char;
  begin
    if b then Result := '1' else Result := '0';
  end;

var
  i, j, r, c  : integer;
  s : String;
begin
  c := LedDisplay.Cols-1;
  r := LedDisplay.Rows-1;
  Memo1.Clear;

  for i := 0 to r do
  begin
    s := '';
    for j := 0 to c do
    begin
      s := s + ToStr(LedDisplay.Leds[i, j].Vis) + ' ';
    end;
    Memo1.Lines.Add(s);
  end;
end;

procedure TgForm.SpinEditColChange(Sender: TObject);
begin
   LedDisplay.Cols := SpinEditCol.Value;
end;

procedure TgForm.SpinEditRowChange(Sender: TObject);
begin
  LedDisplay.Rows := SpinEditRow.Value;
end;

procedure TgForm.SynEdit1GutterClick(Sender: TObject; Button: TMouseButton; X, Y, Line: Integer; Mark: TSynEditMark);
begin
  if FDebugger <> nil then FDebugger.ToggleBreakpoint(SynEdit1.RowToLine(Line));
end;

procedure TgForm.SynEdit1GutterGetText(Sender: TObject; aLine: Integer; var aText: string);
begin
  if aLine = TSynEdit(Sender).CaretY then Exit;

  if aLine mod 10 <> 0 then
    if aLine mod 5 <> 0 then
      aText := '�'
    else
      aText := '-';
end;

procedure TgForm.SynEdit1SpecialLineColors(Sender: TObject; Line: Integer; var Special: Boolean; var FG, BG: TColor);
var
  LI: TDebuggerLineInfos;
begin
  if FDebugger <> nil then begin
    LI := FDebugger.GetLineInfos(Line);
    if dlCurrentLine in LI then begin
      Special := TRUE;
      FG := clWhite;
      BG := clBlue;
    end else if dlBreakpointLine in LI then begin
      Special := TRUE;
      FG := clWhite;
      if dlExecutableLine in LI then
        BG := clRed
      else
        BG := clGray;
    end;
  end;
end;

procedure TgForm.SynEdit1StatusChange(Sender: TObject; Changes: TSynStatusChanges);
begin
  // caret position has changed
  if Changes * [scAll, scCaretX, scCaretY] <> [] then
  begin
    LabelCaretPos.Caption := Format('Ln:%' + SynEdit1.Gutter.DigitCount.ToString + 'd   Col:%3d', [SynEdit1.CaretY, SynEdit1.CaretX]);
  end;
end;

procedure TgForm.ToggleSwitch1Click(Sender: TObject);
var
  m : TByteArray;
  c : Word;
begin
  if ToggleSwitch1.State = tssOn then
  begin
    BComPort1.Port := cbPort.Text;
    if BComPort1.Open then
    begin
      SpinEditRow.Enabled := false;
      SpinEditCol.Enabled := false;
      Sleep(1600);
      cmd.PrepareToSendLedSize(m, c);
      BComPort1.Write(m, c);
      SendHeadLvlTowerAssign;
    end
    else
    begin
      ToggleSwitch1.State := tssOff;
      SpinEditRow.Enabled := true;
      SpinEditCol.Enabled := true;
    end;
  end
  else
  begin
    if BComPort1.Close then
    begin
      SpinEditRow.Enabled := true;
      SpinEditCol.Enabled := true;
    end;
  end;
end;

procedure TgForm.ToolButton9Click(Sender: TObject);
begin
  ShowMessage('script');
end;

procedure TgForm.UpdateVolume;
var
  VolLevel: Single;
begin
  if FEndPoint = nil then Exit;
  FEndPoint.GetMasterVolumeLevelScalar(VolLevel);
  Volume.Position := Round(VolLevel * 100);
end;

procedure TgForm.VolumeChange(Sender: TObject);
begin
  if FVolumeUpdating then Exit;
  FEndPoint.SetMasterVolumeLevelScalar(Volume.Position / 100, nil);
end;

{ TMyEndpointVolumeCallback }

function TMyEndpointVolumeCallback.OnNotify(pNotify: PAUDIO_VOLUME_NOTIFICATION_DATA): HRESULT;
begin
  Result := S_OK;

  gForm.FVolumeUpdating := True;
  try
    gForm.Volume.Position := Round(pNotify.fMasterVolume * 100);
  finally
    gForm.FVolumeUpdating := False;
  end;
end;

{ TFilter }

procedure TFilter.Calc(Left : Integer; Right : Integer = -1);
begin
//  if LeftCurrent = 0 then LeftCurrent := 1;
  LeftAbsolut := Left;    //�� ����� �������� ����� �����
  CountSkiped := (gForm.FilterTimeSpinEdit.Value div 50);  //���������� ����� (���������� ����������)
  LeftStep := (LeftAbsolut - LeftCurrent) / CountSkiped;
end;

procedure TFilter._Calc;
var
  needSteps : Word;  //���������� ����������� �����
  d, LeftLvlAbsTower, LeftLvlCurrentTower :Word;
label l1;
begin
  LeftAbsolut := LeftLvl; //�� ����� �������� ����� �����
  if LeftLvlMax > LeftLvl then LeftLvl := LeftLvlMax; //���� ������������� ������� ���� ��� ���������

//����� ����� ����� � ������ ����
  if gForm.FilterBoxRezkoUp.Checked then
    if LeftLvl > LeftCurrent then
    begin
      gForm.FilterVolumeTimer.Interval := 15;
      Skiped := 1;
      CountSkiped := 2;
      goto l1;
    end;


  if gForm.SoundSpectrum.BitPerSample = 16 then d := 32767
    else if gForm.SoundSpectrum.BitPerSample = 8 then d := 128;
  LeftLvlAbsTower := ConToReal(LeftAbsolut, 0, d, 0, gForm.LedDisplay.Rows);
  LeftLvlCurrentTower := ConToReal(LeftCurrent, 0, d, 0, gForm.LedDisplay.Rows);

  needSteps := LeftLvlAbsTower - LeftLvlCurrentTower;
  if needSteps >= MaxSteps then //���� �������� ����� ���������� ������ ��� ��������� ������ ������� ������
  begin
    gForm.FilterVolumeTimer.Interval := 15;
  end
  else
  begin
    if needSteps = 0 then gForm.FilterVolumeTimer.Interval := 50
      else gForm.FilterVolumeTimer.Interval := gForm.FilterTimeSpinEdit.Value div needSteps;
  end;

  CountSkiped := (gForm.FilterTimeSpinEdit.Value div gForm.FilterVolumeTimer.Interval);  //���������� �����
l1:
  LeftStep := (LeftAbsolut - LeftCurrent) / CountSkiped;
end;

end.
