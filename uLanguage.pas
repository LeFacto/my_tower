unit uLanguage;

interface

uses
  System.SysUtils, Windows, System.Generics.Collections;

type
  TLang = (Rus, Eng);

var
  ListError   : TDictionary<string, string>;
  ListErrorRus: TDictionary<string, string>;
  ListErrorEng: TDictionary<string, string>;

  procedure SetLanguage(lang : TLang);

implementation

procedure SetLanguage(lang : TLang);
begin
  case lang of
    Rus :
    begin
      ListError := ListErrorRus;
    end;

    Eng :
    begin
      ListError := ListErrorEng;
    end;
  end;
end;

procedure AddStrings;
begin
  ListErrorRus.Add('CaptionCompileError', '������ ����������');
  ListErrorEng.Add('CaptionCompileError', 'Error compiled');

  ListErrorRus.Add('UnknownCommand', '������ ����������, �� ��������� �������');
//  ListErrorEng.Add('CycleParam', '');

  ListErrorRus.Add('Unknown', '�� ��������� ������');
//  ListErrorEng.Add('CycleParam', '');

  ListErrorRus.Add('CycleParam', '�������������� ���������� �������� Cycle');
//  ListErrorEng.Add('CycleParam', '');

//  ListErrorRus.Add('CycleRepeatIn', '� ������� Cycle ������������ �������� ��������� ������� Cycle');
//  ListErrorEng.Add('CycleRepeatIn', '');

  ListErrorRus.Add('GoSubParam', '�������������� ���������� �������� GoSub');
//  ListErrorEng.Add('GoSubParam', '');

  ListErrorRus.Add('LedRowParam', '�������������� ���������� ������� led_row');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('LedColParam', '�������������� ���������� ������� led_col');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('LedColCountParam', '�������� �������� ����������� � ������� led_col �� 1 �� 65 535');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('LedRowCountParam', '�������� ����� ����������� � ������� led_row �� 1 �� 65 535');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('LedParam', '�������������� ���������� ������� led');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('LedFewParam', '��������� ������ ��� ����� � ������� led');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('WaitParam', '�������������� ���������� ������� sleep');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('SleepParam', '�������������� ���������� ������� sleep');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('NumParam', '������ �������� ������� �� �������� ������');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('ZeroParam', '�������� ����� � �������� �� ����� ���� ������ 1 ������������');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('Param', '�������������� ����������');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('LineParam', '���������� ����� ������ ���� �� 1 �� ');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('ColumnParam', '���������� �������� ������ ���� �� 1 �� ');
//  ListErrorEng.Add('', '');


  ListErrorRus.Add('check_1_0_x', '�������������� ���������');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('RowColParam', '�� ����� ����� �������� ������ ��� �������');
//  ListErrorEng.Add('', '');

  ListErrorRus.Add('ParamSkipSeveral', '������������ �������� � ������� skip ����� �� ������ skip, wait. ��� ��� ����������� ������� {skip');
//  ListErrorEng.Add('', '');

//  ListErrorRus.Add('', '');
//  ListErrorEng.Add('', '');

end;

initialization
  ListErrorRus := TDictionary<string, string>.Create;
  ListErrorEng := TDictionary<string, string>.Create;
  SetLanguage(Rus);
  AddStrings;

finalization
  ListErrorRus.Free;   ListErrorRus.Clear;
  ListErrorEng.Free;   ListErrorEng.Clear;
end.
