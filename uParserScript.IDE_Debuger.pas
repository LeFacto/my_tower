unit uParserScript.IDE_Debuger;
{$I SynEdit.inc}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.Generics.Collections, System.Generics.Defaults,
  Vcl.Dialogs, LedDisplay, uLanguage;


type
  TSampleDebugger = class;
//  TOnSetRowLedEvent = procedure (Sender: TObject; CountRowLed : String) of object;
//  TOnSetColLedEvent = procedure (Sender: TObject; CountColLed : String) of object;

  TParserStatus = (psCompiled, psRun, psStep);

  TSampleExecutableLine = class
    Line: integer;
    Delta: integer; // to change the array index
  end;

  TLedStateMemory = (lsmRow, lsmCol, lsmAll, lsmDot);
  TLedMemory = class
  public
    state : TLedStateMemory;
    list : TList<Word>;
    Leds : array of array of TLeds;
    NameParam : String;
    constructor Create(AState : TLedStateMemory; AList :TList<Word>; CountRowCol : Word; const ANameParam : String);
    destructor Destroy; override;
  end;

  TParserScript = class
  private
    OriginalScript : TStrings;
    script : TStringList;
    ExecutableLines : TList<integer>;            //������ ����� �������� "�����"
    SampleCode : TObjectList<TSampleExecutableLine>;   //������ ������� ����� ������� ���������� ����
    FError : Boolean;
    FErrorString : String;
    LedDisplay : TLedDisplay;
    fSkipSeveral : Boolean;
    fSkip : Word;
    fRepeat : Word;
    fLedMemory : TObjectList<TLedMemory>;
    function TextToWord(const str : String; var sl : TStringList) :Boolean;
    function TextToWord_LowerCase_Trim(const str : String; var sl : TStringList) :Boolean;
    procedure ParseMain;

    procedure ParseCommand(var cmd: TStringList; var in_i : integer);
    procedure ParseGoComand(const s_cmd : String);

    function check_1_0_X(const str : String): Boolean;
    function line  (const str : String; out lw : TList<Word>): Boolean;
    function Column(const str : String; out cw : TList<Word>): Boolean;
    procedure LedAlignRow(line: SmallInt; var s: String);
    procedure LedAlignCol(Col: SmallInt; var s: String);

    procedure SetRow(var cmd: TStringList);
    procedure SetCol(var cmd: TStringList);
    procedure GoSub(var cmd: TStringList);
    procedure GotoCycle(var cmd: TStringList; var in_i : Integer);
    procedure Led(var cmd: TStringList);
    procedure SleepCMD(var cmd: TStringList);
    procedure WaitCMD(var cmd: TStringList);                 //������ ���� ������� ����� ���������� ��������� ������� ������� ������� sleep �����������
    procedure Skip(var cmd: TStringList; var in_i : integer);//��������� ��������� ������� ��� �����
    procedure ROL_SSL_NSL(var cmd : TStringList; RW : Word); //����������� ����� �����,  ����������� ����� �����  � ����������� 1,  ����������� ����� �����  � ����������� 0
    procedure ROR_SSR_NSR(var cmd : TStringList; RW : Word); //����������� ����� ������, ����������� ����� ������ � ����������� 1,  ����������� ����� ������ � ����������� 0
    procedure ROU_SSU_NSU(var cmd : TStringList; RW : Word); //����������� ����� �����,  ����������� ����� �����  � ����������� 1,  ����������� ����� �����  � ����������� 0
    procedure ROD_SSD_NSD(var cmd : TStringList; RW : Word); //����������� ����� ����,   ����������� ����� ����   � ����������� 1,  ����������� ����� ����   � ����������� 0
    procedure Or_Xor_Not_And_RowCol(var cmd: TStringList; RW : Word);
    procedure SaveLed(var cmd : TStringList);    //��������� ��������� ����������� � ������
    procedure LoadLed(var cmd : TStringList);    //������������ �� ������ ��������� �����������
    procedure ClearLed(var cmd : TStringList);   //������� �� ������ ��������� �����������
    function FindLedMemory(const param :String; out ALedMem : TLedMemory): Boolean;
  public
    Status : TParserStatus;
    hwndForMsgBox : hwnd;
    Sleep : Word;
    Wait : Word;
    constructor Create(ALedDisplay : TLedDisplay; AScript : TStrings; AExecutableLines : TList<integer>; ASampleCode : TObjectList<TSampleExecutableLine>);
    destructor Destroy; override;
    procedure ParseExecutableLines;
    procedure ParseSampleCode(AParserStatus :TParserStatus = psCompiled);
  public
    property Error : Boolean read FError;
  end;


  TDebuggerState = (dsStopped, dsRunning, dsPaused);

  TDebuggerLineInfo = (dlCurrentLine, dlBreakpointLine, dlExecutableLine);
  TDebuggerLineInfos = set of TDebuggerLineInfo;

  TBreakpointChangeEvent = procedure(Sender: TObject; ALine: integer) of object;
  TDebuggerStateChangeEvent = procedure(Sender: TObject; OldState, NewState: TDebuggerState) of object;
  TOnSendStateLedTower = procedure (Sender: TObject) of object;

  TSampleDebugger = class(TObject)
  private
    fBreakpoints: TList;
    fCurrentLine: integer;
    fDebuggerState: TDebuggerState;
    fLineToStop: integer;
    fNextInstruction: integer;
    fWantedState: TDebuggerState;
    fOnBreakpointChange: TBreakpointChangeEvent;
    fOnCurrentLineChange: TNotifyEvent;
    fOnStateChange: TDebuggerStateChangeEvent;
    fOnYield: TNotifyEvent;
    fOnSendStateLedTower :TOnSendStateLedTower;
    function CurrentLineIsBreakpoint: boolean;
    procedure DoOnBreakpointChanged(ALine: integer);
    procedure DoCurrentLineChanged;
    procedure DoStateChange;
    procedure DoYield;
  public
    constructor Create;
    destructor Destroy; override;
    function CanGotoCursor(ALine: integer): boolean;
    function CanPause: boolean;
    function CanRun: boolean;
    function CanStep: boolean;
    function CanStop: boolean;
    procedure ClearAllBreakpoints;
    function GetLineInfos(ALine: integer): TDebuggerLineInfos;
    procedure GotoCursor(ALine: integer);
    function HasBreakpoints: boolean;
    function IsBreakpointLine(ALine: integer): boolean;
    function IsExecutableLine(ALine: integer): boolean;
    function IsRunning: boolean;
    procedure Pause;
    procedure Run;
    procedure Step;
    procedure Stop;
    procedure ToggleBreakpoint(ALine: integer);
  public
    ExecutableLines : TList<integer>;
    SampleCode : TObjectList<TSampleExecutableLine>;
    Parser : TParserScript;
    property CurrentLine: integer read fCurrentLine;
    property OnBreakpointChange: TBreakpointChangeEvent read fOnBreakpointChange write fOnBreakpointChange;
    property OnCurrentLineChange: TNotifyEvent read fOnCurrentLineChange  write fOnCurrentLineChange;
    property OnStateChange: TDebuggerStateChangeEvent read fOnStateChange write fOnStateChange;
    property OnYield: TNotifyEvent read fOnYield write fOnYield;
  published
    property OnSendStateLedTower : TOnSendStateLedTower read fOnSendStateLedTower write fOnSendStateLedTower;
  end;

  function SampleExecutableLine(ALine, ADelta : Integer) : TSampleExecutableLine;

implementation

const
  EndCommand = '~';
  ReserverWord  : array [1..37] of String = ('script', 'led_col', 'led_row', 'main', 'sub', 'ret', 'gosub', 'call', 'label', 'cycle:', 'led', 'sleep', 'skip',
    'ror', 'rol', 'ssl', 'nsl', 'ssr', 'nsr', 'rou', 'ssu', 'nsu', 'rod', 'ssd', 'nsd', 'or', 'or_col', 'xor', 'xor_col', 'not', 'not_col', 'and', 'and_col',
    'wait', 'saveled', 'loadled', 'clearled');

  RW_script   = 1;    RW_led      = 11;   RW_SSU      = 21;    RW_NOT_COL  = 31;
  RW_led_col  = 2;    RW_sleep    = 12;   RW_NSU      = 22;    RW_AND_ROW  = 32;
  RW_led_row  = 3;    RW_skip     = 13;   RW_ROD      = 23;    RW_AND_COL  = 33;
  RW_main     = 4;    RW_ROR      = 14;   RW_SSD      = 24;    RW_wait     = 34;
  RW_sub      = 5;    RW_ROL      = 15;   RW_NSD      = 25;    RW_save_led = 35;
  RW_ret      = 6;    RW_SSL      = 16;   RW_OR_ROW   = 26;    RW_load_led = 36;
  RW_gosub    = 7;    RW_NSL      = 17;   RW_OR_COL   = 27;    RW_clear_led= 37;
  RW_call     = 8;    RW_SSR      = 18;   RW_XOR_ROW  = 28;
  RW_label    = 9;    RW_NSR      = 19;   RW_XOR_COL  = 29;
  RW_cycle    = 10;   RW_ROU      = 20;   RW_NOT_ROW  = 30;

function SampleExecutableLine(ALine, ADelta : Integer) : TSampleExecutableLine;
begin
  Result := TSampleExecutableLine.Create;
  Result.Line := ALine;
  Result.Delta := ADelta;
end;

{ TSampleDebugger }

constructor TSampleDebugger.Create;
begin
  inherited Create;
  fBreakpoints := TList.Create;
  fCurrentLine := -1;
  fDebuggerState := dsStopped;

  SampleCode := TObjectList<TSampleExecutableLine>.Create;
  SampleCode.Add(SampleExecutableLine(1, 0));
  fNextInstruction := SampleCode.Count-1;
  ExecutableLines := TList<integer>.Create;
end;

destructor TSampleDebugger.Destroy;
begin
  fBreakpoints.Free;
  FreeAndNil(ExecutableLines);
  FreeAndNil(SampleCode);
  inherited Destroy;
end;

function TSampleDebugger.CanGotoCursor(ALine: integer): boolean;
begin
  Result := (fDebuggerState <> dsRunning) and IsExecutableLine(ALine);
end;

function TSampleDebugger.CanPause: boolean;
begin
  Result := fDebuggerState = dsRunning;
end;

function TSampleDebugger.CanRun: boolean;
begin
  Result := fDebuggerState <> dsRunning;
end;

function TSampleDebugger.CanStep: boolean;
begin
  Result := fDebuggerState <> dsRunning;
end;

function TSampleDebugger.CanStop: boolean;
begin
  Result := fDebuggerState <> dsStopped;
end;

function TSampleDebugger.CurrentLineIsBreakpoint: boolean;
begin
  Result := (fCurrentLine = fLineToStop)
    or ((fBreakpoints.Count > 0) and IsBreakpointLine(fCurrentLine));
end;

procedure TSampleDebugger.DoOnBreakpointChanged(ALine: integer);
begin
  if Assigned(fOnBreakpointChange) then
    fOnBreakpointChange(Self, ALine);
end;

procedure TSampleDebugger.DoCurrentLineChanged;
begin
  if Assigned(fOnCurrentLineChange) then
    fOnCurrentLineChange(Self);
end;

procedure TSampleDebugger.DoStateChange;
begin
  if fDebuggerState <> fWantedState then
  begin
    if fWantedState = dsStopped then fCurrentLine := -1;
    if Assigned(fOnStateChange) then fOnStateChange(Self, fDebuggerState, fWantedState);
    fDebuggerState := fWantedState;
    if fWantedState <> dsRunning then fLineToStop := -1;
    DoCurrentLineChanged;
  end;
end;

procedure TSampleDebugger.DoYield;
begin
  if Assigned(fOnYield) then
    fOnYield(Self);
end;

procedure TSampleDebugger.ClearAllBreakpoints;
begin
  if fBreakpoints.Count > 0 then begin
    fBreakpoints.Clear;
    DoOnBreakpointChanged(-1);
  end;
end;

function TSampleDebugger.GetLineInfos(ALine: integer): TDebuggerLineInfos;
begin
  Result := [];
  if ALine > 0 then begin
    if ALine = fCurrentLine then    Include(Result, dlCurrentLine);

    if IsExecutableLine(ALine) then Include(Result, dlExecutableLine);

    if IsBreakpointLine(ALine) then Include(Result, dlBreakpointLine);
  end;
end;

procedure TSampleDebugger.GotoCursor(ALine: integer);
begin
  fLineToStop := ALine;
  Run;
end;

function TSampleDebugger.HasBreakpoints: boolean;
begin
  Result := fBreakpoints.Count > 0;
end;

function TSampleDebugger.IsBreakpointLine(ALine: integer): boolean;
var
  i: integer;
begin
  Result := FALSE;
  if ALine > 0 then begin
    i := fBreakpoints.Count - 1;
    while i >= 0 do begin
      if integer(fBreakpoints[i]) = ALine then begin
        Result := TRUE;
        break;
      end;
      Dec(i);
    end;
  end;
end;

function TSampleDebugger.IsExecutableLine(ALine: integer): boolean;
var
  i: integer;
begin
  Result := FALSE;
  if ALine > 0 then begin
//    i := High(ExecutableLines);
    i := ExecutableLines.Count-1;
//    while i >= Low(ExecutableLines) do begin
    while i >= 0 do begin
      if ALine = ExecutableLines[i] then begin
        Result := TRUE;
        break;
      end;
      Dec(i);
    end;
  end;
end;

function TSampleDebugger.IsRunning: boolean;
begin
  Result := fDebuggerState = dsRunning;
end;

procedure TSampleDebugger.Pause;
begin
  if fDebuggerState = dsRunning then
    fWantedState := dsPaused;
end;

procedure TSampleDebugger.Run;
var
  dwTime: DWORD;
  label next;
begin
  fNextInstruction := 0; //SampleCode.Count-1 ;
  fCurrentLine := SampleCode[fNextInstruction].Line;
  fWantedState := dsRunning;
  DoStateChange;
  dwTime := GetTickCount + 500;
  Parser.fSkip := 0;
  Parser.fRepeat := 0;
  repeat
    if GetTickCount >= dwTime then begin
      DoYield;
      if Parser.Wait <> 0 then //������������ ��������
      begin dwTime := GetTickCount + Parser.Wait; Parser.Wait := 0; end
      else dwTime := GetTickCount + Parser.Sleep;

    next:
      Step;
      if Parser.fSkip > 0 then begin Dec(Parser.fSkip); goto next; end;
      if Parser.fSkipSeveral then goto next;

      Parser.LedDisplay.PaintOnlyLed;
      if Assigned(fOnSendStateLedTower) then
        fOnSendStateLedTower(Self);

      if fWantedState <> fDebuggerState then DoStateChange;
    end;
  until fDebuggerState <> dsRunning;

  fLineToStop := -1;
end;

procedure TSampleDebugger.Step;
var
  fNextInstructionMem : Integer;
begin
  if fDebuggerState = dsStopped then begin
    fNextInstruction := 0; //SampleCode.Count-1;
    fCurrentLine := SampleCode[fNextInstruction].Line;
    fWantedState := dsPaused;
    DoStateChange;
  end
  else
  begin
//    Sleep(50);
    fNextInstructionMem  := fNextInstruction;
    if fNextInstruction = 0 then
      Parser.fLedMemory.Clear;

    Parser.ParseGoComand(Parser.Script[fCurrentLine-1]); //���������� �������
    fNextInstruction := fNextInstruction + SampleCode[fNextInstruction].Delta;
    fCurrentLine := SampleCode[fNextInstruction].Line;

    if Parser.fRepeat > 0 then
      begin
        Dec(Parser.fRepeat);
        if Parser.fRepeat <> 0 then
        begin
          fNextInstruction := fNextInstructionMem;
          fCurrentLine := SampleCode[fNextInstruction].Line;
        end;
      end;

    case fDebuggerState of
      dsRunning:
        begin
          if CurrentLineIsBreakpoint then fWantedState := dsPaused;
        end;
    else
      DoCurrentLineChanged;
    end;
  end;
end;

procedure TSampleDebugger.Stop;
begin
  fWantedState := dsStopped;
  DoStateChange;
end;

procedure TSampleDebugger.ToggleBreakpoint(ALine: integer);
var
  SetBP: boolean;
  i: integer;
begin
  if ALine > 0 then begin
    SetBP := TRUE;
    for i := 0 to fBreakpoints.Count - 1 do begin
      if integer(fBreakpoints[i]) = ALine then begin
        fBreakpoints.Delete(i);
        SetBP := FALSE;
        break;
      end else if integer(fBreakpoints[i]) > ALine then begin
        fBreakpoints.Insert(i, pointer(ALine));
        SetBP := FALSE;
        break;
      end;
    end;
    if SetBP then
      fBreakpoints.Add(pointer(ALine));
    DoOnBreakpointChanged(ALine);
  end;
end;


{ TParserScript }

function TParserScript.check_1_0_X(const str: String): Boolean;
var
  i : Integer;
begin
  Result := true;
  for i := Low(str) to High(str) do
    if (str[i] <> '1')and(str[i] <> '0')and(str[i] <> 'x')and(str[i] <> '!') then
    begin
      Result := false;
      FError := true;
      ListError.TryGetValue('check_1_0', FErrorString);
      FErrorString := FErrorString + ' "' + str + '"';
      Break;
    end;
end;

procedure TParserScript.ClearLed(var cmd: TStringList);
var
  LedMem : TLedMemory;
  i, c : Integer;
  ResultFind : Boolean;
begin
  if Status = psCompiled then
  begin
    if cmd.Count <> 2 then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
  end;

  if Status = psRun then
  begin
    c := fLedMemory.Count-1;
    ResultFind := false;
    for i := 0 to c do
      if fLedMemory[i].NameParam = cmd[1] then
      begin
        ResultFind := true;
        break;
      end;
      if ResultFind then fLedMemory.Delete(i);// else begin ShowMessage('�� ������ ��������� ������ ' + cmd[1]); end;
  end;
end;

function TParserScript.Column(const str: String; out cw: TList<Word>): Boolean;
var
  r_column : real;
  c1, i, c, l1, l2 : Integer;
begin
  Result := true;
  cw := TList<Word>.Create;

  if str = 'first' then
  begin
    cw.Add(0);
    exit;
  end
//  else
//  if str = 'center' then
//  begin
//      r_column := LedDisplay.Rows / 2;
//      if Frac(r_column) = 0 then begin cw.Add(Trunc(r_column)-1); cw.Add(Trunc(r_column)); end
//                          else cw.Add(Trunc(r_column));
//      exit;
//  end
//  else
//  if str = 'last' then
//  begin
//    cw.Add(LedDisplay.Rows-1);
//    exit;
//  end
  else
  if str = 'all' then
  begin
    c := LedDisplay.Cols-1;
    for i := 0 to c do cw.Add(i);
  end
  else
  if Pos('-', str) > 0 then //�������� ��������
  begin
    i := Pos('-', str);
    if not TryStrToInt(Copy(str, 1, i-1), l1) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    if not TryStrToInt(Copy(str, i+1, str.Length-i), l2) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    Dec(l1); Dec(l2);
    if (l1 < 0)or(l1 > l2)or(l2 > LedDisplay.Cols-1) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    for i := l1 to l2 do cw.Add(i);
  end
  else
  if Pos('step_', str) > 0 then //�������� �������� � �������� �����
  begin
    if not TryStrToInt(Copy(str, 6, str.Length-5), l1) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    i := 0;
    while i <= LedDisplay.Cols-1 do
    begin
      cw.Add(i);
      Inc(i, l1);
    end;
  end
  else
  if Pos('step1_', str) > 0 then //�������� �������� � �������� �����
  begin
    if not TryStrToInt(Copy(str, 7, str.Length-6), l1) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    i := l1-1;
    while i <= LedDisplay.Cols-1 do
    begin
      cw.Add(i);
      Inc(i, l1);
    end;
  end
  else

  if not TryStrToInt(str, c1) then //���������� �������
  begin
    FError := true; ListError.TryGetValue('Param', FErrorString);
    exit;
  end
  else
  begin
    Dec(c1);
    if (c1 < 0)or(c1 > LedDisplay.Cols-1) then
    begin
      Result := false;
      FError := true;
      ListError.TryGetValue('ColumnParam', FErrorString);
      FErrorString := FErrorString + LedDisplay.Cols.ToString;
      exit;
    end;
    cw.Add(c1);
  end;

end;

constructor TParserScript.Create;
begin
  LedDisplay := ALedDisplay;
  OriginalScript := AScript;
  ExecutableLines := AExecutableLines;
  SampleCode := ASampleCode;
  FError := false;
  script := TStringList.Create;
  Sleep := 100;
  Wait := 0;
  fLedMemory := TObjectList<TLedMemory>.Create;
end;

destructor TParserScript.Destroy;
begin
  FreeAndNil(script);
  FreeAndNil(fLedMemory);
  inherited;
end;

function TParserScript.FindLedMemory(const param: String; out ALedMem : TLedMemory): Boolean;
var
  LedMem : TLedMemory;
begin
  Result := false;

  for LedMem in fLedMemory do
    if LedMem.NameParam = param then
    begin
      ALedMem := LedMem;
      Result := true;
      break;
    end;
end;

procedure TParserScript.GotoCycle(var cmd: TStringList; var in_i : integer);
var
  count  : Integer; //���������� ��������
  ii, c, c_add, j : Integer;
  sl : TStringList;
  curExecutableLines : Integer;
begin
//CYCLE: <count>       ; Define Label "CYCLE"
//...
//goto CYCLE    ; Infinite cycle
  if (cmd.Count = 1) or (cmd.Count > 2) or not TryStrToInt(cmd[1], count) then
  begin
    FError := true;
    ListError.TryGetValue('CycleParam', FErrorString);
    Exit;
  end;

  if count <= 0 then
  begin
    FError := true;
    ListError.TryGetValue('CycleParam', FErrorString);
    Exit;
  end;

  c_add := SampleCode.Count;
  sl := TStringList.Create;
  c := ExecutableLines.Count-1;

  for j := 0 to count-1 do //���������� ��������
    for ii := in_i+1 to c do  //����� ����� � ������ �������
    begin
      curExecutableLines := ExecutableLines[ii]-1;
      if not TextToWord(script[curExecutableLines], sl) then Continue else
      begin
//        ShowMessage(script[curExecutableLines]);
        if script[curExecutableLines] = 'goto cycle' then
        begin
          //
          SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
          break;
        end;
        ParseCommand(sl, ii);
      end;
    end;


  in_i := ii;
  FreeAndNil(sl);
end;

procedure TParserScript.Led(var cmd: TStringList);
  procedure Led_Mask(s: String; line: SmallInt);
  var
    len, i, j, c : SmallInt;
    b : Char;
  begin
    len := Length(s);

    if LedDisplay.Cols < len then len := LedDisplay.Cols
    else
    begin
      c := LedDisplay.Cols - len;
      b := s[len];
      for i := 1 to c do s := s + b;
      len := LedDisplay.Cols;
    end;
//
    Dec(line);
    for i := 1 to len do
    case s[i] of
      '1' : LedDisplay.Leds[line][i-1].Vis := true;
      '0' : LedDisplay.Leds[line][i-1].Vis := false;
      '!' : LedDisplay.Leds[line][i-1].Vis := not LedDisplay.Leds[line][i-1].Vis;
    end;
  end;

  procedure led_align_row(var cmd: TStringList);
  begin
    if cmd.Count = LedDisplay.Rows + 2 then exit;
    while cmd.Count >  LedDisplay.Rows + 2 do cmd.Delete(cmd.Count-1);
    while cmd.Count <= LedDisplay.Rows + 2 do cmd.Add(cmd[cmd.Count-1]);
  end;


var
  r, i, lenLed : integer;
  r_line : Real;
  lw : TList<Word>;
  str : String;
begin
//led 1 1!xxxx0101111
//led 2 1111111111111111
//led 0 1111111111111111
//led [] 1111111111111111 1111111111111111
//led all 1

  if Status = psCompiled then
  begin
    if cmd.Count = 2 then
    begin
      if cmd[1] <> 'off' then
        if cmd[1] <> 'on' then begin FError := true; ListError.TryGetValue('LedParam', FErrorString); exit; end;
      exit;
    end// if cmd.Count = 2 then
    else
    if cmd.Count < 3 then begin FError := true; ListError.TryGetValue('LedParam', FErrorString); exit; end
    else
    if cmd[1] = '[]' then
    begin
//      if cmd.Count <> LedDisplay.Rows + 2 then begin FError := true; ListError.TryGetValue('LedFewParam', FErrorString); exit; end;
    end
    else
    if not check_1_0_X(cmd[2]) then begin exit; end;
  end;


//���������� �������
  if Status = psRun then
  begin
    if cmd.Count = 2 then
    begin
      if (cmd[1] = 'off') then LedDisplay.LED_off;
      if (cmd[1] = 'on') then  LedDisplay.LED_on;
    end
    else
    if cmd[1] = '[]' then
    begin
      led_align_row(cmd);

      r := Length(LedDisplay.Leds);
      for i := 1 to r do led_mask(cmd[i+1], i);
    end
    else
    begin
      if not line(cmd[1], lw) then begin lw.Free; exit; end;

      for r in lw do //���������� ����� �� ����������
      begin
        str := cmd[2];
        LedAlignRow(r, str);
        lenLed := Length(LedDisplay.Leds[r])-1;

        for i := 0 to lenLed do
        case str[i+1] of
          '1' : LedDisplay.Leds[r][i].Vis := true;
          '0' : LedDisplay.Leds[r][i].Vis := false;
          '!' : LedDisplay.Leds[r][i].Vis := not LedDisplay.Leds[r][i].Vis;
        end;
      end;

      lw.Free;
    end;
  end;
end;

procedure TParserScript.LedAlignCol(Col: SmallInt; var s: String);
var
  lenS, lenLed, i, j, c : SmallInt;
  b : Char;
begin
  lenS := Length(s);
  lenLed := LedDisplay.Rows; //��� �������� � �������

  if lenLed > lenS then
  begin
    c := lenLed - lenS;
    b := s[lenS];
    for i := 1 to c do s := s + b;
  end;
end;

procedure TParserScript.LedAlignRow(line: SmallInt; var s: String);
var
  lenS, lenLed, i, j, c : SmallInt;
  b : Char;
begin
  lenS := Length(s);
  lenLed := Length(LedDisplay.Leds[line]);

  if lenLed > lenS then
  begin
    c := lenLed - lenS;
    b := s[lenS];
    for i := 1 to c do s := s + b;
  end;
end;

function TParserScript.line(const str: String; out lw : TList<Word>): Boolean;
var
  r_line : real;
  l1, l2, i, c : Integer;
begin
  Result := true;
  lw := TList<Word>.Create;

  if str = 'first' then
  begin
    lw.Add(0);
    exit;
  end
  else
  if str = 'center' then
  begin
      r_line := LedDisplay.Rows / 2;
      if Frac(r_line) = 0 then begin lw.Add(Trunc(r_line)-1); lw.Add(Trunc(r_line)); end
                          else lw.Add(Trunc(r_line));
      exit;
  end
  else
  if str = 'last' then
  begin
    lw.Add(LedDisplay.Rows-1);
    exit;
  end
  else
  if str = 'all' then
  begin
    c := LedDisplay.Rows-1;
    for i := 0 to c do lw.Add(i);
  end
  else
  if Pos('-', str) > 0 then //�������� �����
  begin
    i := Pos('-', str);
    if not TryStrToInt(Copy(str, 1, i-1), l1) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    if not TryStrToInt(Copy(str, i+1, str.Length-i), l2) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    Dec(l1); Dec(l2);
    if (l1 < 0)or(l1 > l2)or(l2 > LedDisplay.Rows-1) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    for i := l1 to l2 do lw.Add(i);
  end
  else
  if Pos('step_', str) > 0 then //�������� ����� � �������� �����
  begin
    if not TryStrToInt(Copy(str, 6, str.Length-5), l1) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    i := 0;
    while i <= LedDisplay.Rows-1 do
    begin
      lw.Add(i);
      Inc(i, l1);
    end;
  end
  else
  if Pos('step1_', str) > 0 then //�������� ����� � �������� �����
  begin
    if not TryStrToInt(Copy(str, 7, str.Length-6), l1) then begin FError := true; ListError.TryGetValue('RowColParam', FErrorString); exit; end;
    i := l1-1;
    while i <= LedDisplay.Rows-1 do
    begin
      lw.Add(i);
      Inc(i, l1);
    end;
  end
  else
  if not TryStrToInt(str, l1) then //���������� ������
  begin
    FError := true; ListError.TryGetValue('Param', FErrorString);
    exit;
  end
  else
  begin
    Dec(l1);
    if (l1 < 0)or(l1 > LedDisplay.Rows-1) then
    begin
      Result := false;
      FError := true;
      ListError.TryGetValue('RowColParam', FErrorString);
      FErrorString := FErrorString + LedDisplay.Rows.ToString;
      exit;
    end;
    lw.Add(l1);
  end;
end;

procedure TParserScript.LoadLed(var cmd: TStringList);
var
  i, j, i2, j2  : Word;
  LedMem : TLedMemory;
begin
//LoadLed <name param>
  if Status = psCompiled then
  begin
    if (cmd.Count <> 2) then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
  end;

  if Status = psRun then
  begin
    //����� ������� ��������
    if not FindLedMemory(cmd[1], LedMem) then exit;

    //�������������� �� ������
    if (LedMem.state = lsmRow)or(LedMem.state = lsmAll) then
    begin
      i2 := 0;
      for i in LedMem.list do
      begin
        j2 := 0;
        for j := 0 to LedDisplay.Cols-1 do
        begin
          LedDisplay.Leds[i, j].Vis := LedMem.Leds[i2, j2].Vis;
          LedDisplay.Leds[i, j].Color := LedMem.Leds[i2, j2].Color;
          Inc(j2);
        end;
        Inc(i2);
      end;
    end
    else if LedMem.state = lsmCol then
    begin
      i2 := 0;
      for i in LedMem.list do
      begin
        j2 := 0;
        for j := 0 to LedDisplay.Rows-1 do
        begin
          LedDisplay.Leds[j, i].Vis := LedMem.Leds[j2, i2].Vis;
          LedDisplay.Leds[j, i].Color := LedMem.Leds[j2, i2].Color;
          Inc(j2);
        end;
        Inc(i2);
      end;
    end;

  end;
end;

procedure TParserScript.Or_Xor_Not_And_RowCol(var cmd: TStringList; RW : Word);
var
  lw : TList<Word>;
  cw : TList<Word>;
  str : String;
  i, lenLed, r, c : integer;
  p : byte;
begin
// OR_ROW <row> <light diode state>
// OR_COL <column> <light diode state>
//not <row> <count repeat>
  if Status = psCompiled then
  begin
    if not ((cmd.Count = 3)or(cmd.Count = 4)) then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
    case RW of
      RW_OR_ROW, RW_XOR_ROW, RW_AND_ROW, RW_NOT_ROW : if not line(cmd[1], lw)   then begin lw.Free; exit; end else lw.Free;
      RW_OR_COL, RW_XOR_COL, RW_AND_COL, RW_NOT_COL : if not Column(cmd[1], cw) then begin cw.Free; exit; end else cw.Free;
    end;

    case RW of
      RW_OR_ROW, RW_XOR_ROW, RW_AND_ROW, RW_OR_COL, RW_XOR_COL, RW_AND_COL : if not check_1_0_X(cmd[2]) then begin exit; end;
    end;
  end;

  if Status = psRun then
  begin
    if (RW = RW_NOT_COL)or(RW = RW_NOT_ROW) then p := 2; //else p := 3;
      if fRepeat = 0 then
        if cmd.Count = p+1 then
          if TryStrToInt(cmd[p], c) then
           if c > 1 then fRepeat := c;

    if (RW = RW_OR_ROW)or(RW = RW_XOR_ROW)or(RW = RW_NOT_ROW)or(RW = RW_AND_ROW) then  //� ������
    begin
      line(cmd[1], lw);
      for r in lw do
      begin
        str := cmd[2];
        LedAlignRow(r, str);
        lenLed := Length(LedDisplay.Leds[r])-1;

        case RW of
          RW_OR_ROW  : for i := 0 to lenLed do if str[i+1] <> 'x' then LedDisplay.Leds[r, i].Vis := LedDisplay.Leds[r, i].Vis or  StrToBool(str[i+1]);
          RW_XOR_ROW : for i := 0 to lenLed do if str[i+1] <> 'x' then LedDisplay.Leds[r, i].Vis := LedDisplay.Leds[r, i].Vis xor StrToBool(str[i+1]);
          RW_AND_ROW : for i := 0 to lenLed do if str[i+1] <> 'x' then LedDisplay.Leds[r, i].Vis := LedDisplay.Leds[r, i].Vis and StrToBool(str[i+1]);
          RW_NOT_ROW : for i := 0 to lenLed do LedDisplay.Leds[r, i].Vis := not LedDisplay.Leds[r, i].Vis;
        end;
      end;

      lw.Free;
    end; //if RW = RW_OR_ROW then

    if (RW = RW_OR_COL)or(RW = RW_XOR_COL)or(RW = RW_NOT_COL)or(RW = RW_AND_COL) then //� �������
    begin
      Column(cmd[1], cw);
      for c in cw do
      begin
        str := cmd[2];
        LedAlignCol(c, str); //
        lenLed := LedDisplay.Rows-1; //��� �������� ���� �������� ������

        case RW of
          RW_OR_COL  : for i := 0 to lenLed do if str[i+1] <> 'x' then LedDisplay.Leds[i, c].Vis := LedDisplay.Leds[i, c].Vis or  StrToBool(str[i+1]);
          RW_XOR_COL : for i := 0 to lenLed do if str[i+1] <> 'x' then LedDisplay.Leds[i, c].Vis := LedDisplay.Leds[i, c].Vis xor StrToBool(str[i+1]);
          RW_AND_COL : for i := 0 to lenLed do if str[i+1] <> 'x' then LedDisplay.Leds[i, c].Vis := LedDisplay.Leds[i, c].Vis and StrToBool(str[i+1]);
          RW_NOT_COL : for i := 0 to lenLed do LedDisplay.Leds[i, c].Vis := not LedDisplay.Leds[i, c].Vis;
        end;
      end;

      cw.Free;
    end;

  end;
end;

procedure TParserScript.Skip(var cmd: TStringList; var in_i : integer);
var
  c : integer;

  SeveralCMD : Boolean;
  count  : Integer; //���������� ��������
  ii, c_add, j : Integer;
  sl : TStringList;
  curExecutableLines : Integer;
begin
//Skip <count>

//Skip <count> {
//...
//}Skip    ; End skip
  if Status = psCompiled then
  begin
    SeveralCMD := false;
    count := 1;

    if cmd.Count = 2 then
    begin
      if cmd[1] = '{' then SeveralCMD := true
      else if not TryStrToInt(cmd[1], count) then begin FError := true; ListError.TryGetValue('Param', FErrorString); Exit; end
    end;

    if cmd.Count = 3 then
    begin
      if not TryStrToInt(cmd[1], count) then begin FError := true; ListError.TryGetValue('Param', FErrorString); Exit; end;
      if cmd[2] = '{' then SeveralCMD := true else begin FError := true; ListError.TryGetValue('Param', FErrorString); Exit; end;
    end;

    if SeveralCMD = false then exit;

    c_add := SampleCode.Count;
    sl := TStringList.Create;
    c := ExecutableLines.Count-1;

    for j := 0 to count-1 do //���������� ��������
      for ii := in_i+1 to c do  //����� ����� � ������ �������
      begin
//        if ii = in_i then begin  Continue; end;
        curExecutableLines := ExecutableLines[ii]-1;
        if not TextToWord(script[curExecutableLines], sl) then Continue else
        begin

          if (sl[0] = 'skip')or(sl[0] = 'ret')or(sl[0] = 'wait') then //�������� �� ����������� � �������� �� ������ ���� ��� ����� ������� "}skip"
            begin FError := true; ListError.TryGetValue('ParamSkipSeveral', FErrorString); FreeAndNil(sl); exit; end;

          if script[curExecutableLines] = '}skip' then
          begin
            if j = count-1 then //�������� ���� ��� ��� ��� �� ����� ������� ���� ���������� �������� �����
              SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
            break;
          end;

          ParseCommand(sl, ii);
        end;
      end;

    in_i := ii;
    FreeAndNil(sl);
  end;

  if Status = psRun then
  begin
    fSkipSeveral := false;

    case cmd.Count of
      2 : if cmd[1] = '{' then fSkipSeveral := true;
      3 : if cmd[2] = '{' then fSkipSeveral := true;
    end;

    if cmd.Count = 1 then fSkip := 2 else
      if TryStrToInt(cmd[1], c) then
          if c > 1 then fSkip := c+1;
  end;
end;

procedure TParserScript.GoSub(var cmd: TStringList);
var
  i, c, c_add : Integer;
  str, s : String;

  sl : TStringList;

  LinePosMain : Integer;
  curLine : Integer;
begin
  sl := TStringList.Create;
  c := ExecutableLines.Count-1;
  c_add := 0;

//find word "gosub"
  for i := 0 to c do
  begin
    curLine := ExecutableLines[i]-1;
    if not TextToWord(script[curLine], sl) then Continue else
    begin
      if sl.Count = 0 then begin ShowMessage('Critical error parser. Not found word!'); Exit; end; //ShowMessage(IntToStr(curLine) + '!' +script[curLine]);  ShowMessage('!' + sl[0]);
      if (sl[0] = ReserverWord[RW_sub])and(sl[1] = cmd[1]) then begin
        LinePosMain := i;
        SampleCode.Add(SampleExecutableLine(curLine+1, 1));
        Inc(c_add);
        break;
      end;
    end;
  end;

//find word "ret" and error
  for i := LinePosMain+1 to c do
  begin
    curLine := ExecutableLines[i]-1;
    if not TextToWord(script[curLine], sl) then Continue else
    begin
      if sl.Count = 0 then begin ShowMessage('Critical error parser. Not found word!'); FError := true; Exit; end;

      if sl[0] = ReserverWord[RW_sub] then begin ShowMessage('Error parser. Find sub on main!'); FError := true; Exit; end;

//      SampleCode.Add(SampleExecutableLine(curLine+1, 1));

      if sl[0] = ReserverWord[RW_ret] then
      begin
        SampleCode.Add(SampleExecutableLine(curLine+1, 1));
        SampleCode[SampleCode.Count-1].Delta := SampleCode[c_add].Delta;
        break;
      end;
      ParseCommand(sl, i);   //  <-----------------------------------------------------
      if FError then break;
    end;
  end;

  FreeAndNil(sl);

end;

procedure TParserScript.ParseMain;
var
  i, c, c_main : Integer;
  str, s : String;

  sl : TStringList;

  LinePosMain : Integer;
  curExecutableLines : Integer;
begin
  sl := TStringList.Create;
  c := ExecutableLines.Count-1;
  c_main := 0;
//find word "main"
  for i := 0 to c do
  begin
    curExecutableLines := ExecutableLines[i]-1;
    if not TextToWord(script[curExecutableLines], sl) then Continue else
    begin
      if sl.Count = 0 then begin ShowMessage('Critical error parser. Not found word!'); Exit; end; //ShowMessage(IntToStr(curExecutableLines) + '!' +script[curExecutableLines]);  ShowMessage('!' + sl[0]);
      if sl[0] = ReserverWord[RW_main] then begin
        LinePosMain := i;
        SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
//        c_main := SampleCode.Count;
        inc(c_main);
        break;
      end;
    end;
  end;

//find word "ret" and error
  for i := LinePosMain+1 to c do
  begin
    curExecutableLines := ExecutableLines[i]-1;
    if not TextToWord(script[curExecutableLines], sl) then Continue else
    begin
      if sl.Count = 0 then begin ShowMessage('Critical error parser. Not found word!'); FError := true; Exit; end;

      if sl[0] = ReserverWord[RW_sub] then begin ShowMessage('Error parser. Find sub on main!'); FError := true; Exit; end;

//      SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));

      if sl[0] = ReserverWord[RW_ret] then
      begin
        SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
        SampleCode[SampleCode.Count-1].Delta := c_main-SampleCode.Count;
        break;
      end;
      ParseCommand(sl, i);  //  <-----------------------------------------------------
      if FError then break;
    end;
  end;

  FreeAndNil(sl);
  Status := psRun;
  Sleep := 100;
  Wait := 0;
end;

procedure TParserScript.ParseCommand(var cmd: TStringList; var in_i : integer);
var
  k, c : Integer;
  curExecutableLines : integer;
begin
  curExecutableLines := ExecutableLines[in_i]-1;

  if cmd[0] = ReserverWord[RW_led] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Led(cmd);
    exit;
  end;

  if cmd[0] = ReserverWord[RW_ROL] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROL_SSL_NSL(cmd, RW_ROL); exit; end;
  if cmd[0] = ReserverWord[RW_SSL] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROL_SSL_NSL(cmd, RW_SSL); exit; end;
  if cmd[0] = ReserverWord[RW_NSL] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROL_SSL_NSL(cmd, RW_NSL); exit; end;

  if cmd[0] = ReserverWord[RW_ROR] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROR_SSR_NSR(cmd, RW_ROR); exit; end;
  if cmd[0] = ReserverWord[RW_SSR] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROR_SSR_NSR(cmd, RW_SSR); exit; end;
  if cmd[0] = ReserverWord[RW_NSR] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROR_SSR_NSR(cmd, RW_NSR); exit; end;

  if cmd[0] = ReserverWord[RW_ROU] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROU_SSU_NSU(cmd, RW_ROU); exit; end;
  if cmd[0] = ReserverWord[RW_SSU] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROU_SSU_NSU(cmd, RW_SSU); exit; end;
  if cmd[0] = ReserverWord[RW_NSU] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROU_SSU_NSU(cmd, RW_NSU); exit; end;

  if cmd[0] = ReserverWord[RW_ROD] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROD_SSD_NSD(cmd, RW_ROD); exit; end;
  if cmd[0] = ReserverWord[RW_SSD] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROD_SSD_NSD(cmd, RW_SSD); exit; end;
  if cmd[0] = ReserverWord[RW_NSD] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); ROD_SSD_NSD(cmd, RW_NSD); exit; end;

  if cmd[0] = ReserverWord[RW_sleep] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    SleepCMD(cmd);
    exit;
  end;

  if cmd[0] = ReserverWord[RW_WAIT] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    WaitCMD(cmd);
    exit;
  end;

  if cmd[0] = ReserverWord[RW_skip] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Skip(cmd, in_i);
    exit;
  end;

  if cmd[0] = ReserverWord[RW_gosub] then //gosub FUNCTION1 <count>;
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    if cmd.Count > 3 then begin FError := true; ListError.TryGetValue('GoSubParam', FErrorString); exit; end;

    if cmd.Count = 2 then GoSub(cmd);
    if cmd.Count = 3 then
    begin
      if TryStrToInt(cmd[2], c) then for k := 1 to c do GoSub(cmd)
        else begin FError := true; ListError.TryGetValue('GoSubParam', FErrorString); exit; end;
    end;
    Exit;
  end;   //gosub FUNCTION1 <count>;

  if cmd[0] = ReserverWord[RW_cycle] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    GotoCycle(cmd, in_i);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_OR_ROW] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Or_Xor_Not_And_RowCol(cmd, RW_OR_ROW);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_OR_COL] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Or_Xor_Not_And_RowCol(cmd, RW_OR_COL);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_NOT_ROW] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Or_Xor_Not_And_RowCol(cmd, RW_NOT_ROW);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_NOT_COL] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Or_Xor_Not_And_RowCol(cmd, RW_NOT_COL);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_XOR_ROW] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Or_Xor_Not_And_RowCol(cmd, RW_XOR_ROW);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_XOR_COL] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Or_Xor_Not_And_RowCol(cmd, RW_XOR_COL);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_AND_ROW] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Or_Xor_Not_And_RowCol(cmd, RW_AND_ROW);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_AND_COL] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    Or_Xor_Not_And_RowCol(cmd, RW_AND_COL);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_save_led] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    SaveLed(cmd);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_load_led] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    LoadLed(cmd);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_clear_led] then
  begin
    SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1));
    ClearLed(cmd);
    Exit;
  end;

  if cmd[0] = ReserverWord[RW_led_row] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); SetRow(cmd); Exit; end;
  if cmd[0] = ReserverWord[RW_led_col] then begin SampleCode.Add(SampleExecutableLine(curExecutableLines+1, 1)); SetCol(cmd); Exit; end;

  FError := true;
  ListError.TryGetValue('UnknownCommand', FErrorString);
  FErrorString := FErrorString + #10 + cmd.Text;
end;

procedure TParserScript.ParseExecutableLines;
var
  i : Integer;
  s, sc : String;
  fWord : String;
begin
  FError := false;
  ExecutableLines.Clear;
  i := 0;
  for sc in OriginalScript do
  begin
    Inc(i);
    s := sc.Trim;
    if Length(s) = 0 then Continue;
    if s[1] = ';' then Continue;

    fWord := LowerCase(Copy(s, 1, Pos(' ', s)-1));
    if fWord = ReserverWord[RW_script] then
    begin
      Continue;
    end;

    ExecutableLines.Add(i);
  end;
end;

procedure TParserScript.ParseGoComand(const s_cmd: String);
var
  z : integer;
  cmd : TStringList;
  label gn;
begin
//  fNoSleep := false; //����� �� ��������� ��������� ������� ���������������
  cmd := TStringList.Create;
  if not TextToWord(s_cmd, cmd) then begin FreeAndNil(cmd); goto gn; end;

  if (cmd[0] = ReserverWord[RW_main]) or (cmd[0] = ReserverWord[RW_ret]) or (cmd[0] = ReserverWord[RW_sub]) or (cmd[0] = ReserverWord[RW_gosub]) then
  begin fSkip := 1; goto gn; end;

  if cmd[0] = ReserverWord[RW_led]   then begin Led(cmd);                 goto gn; end;
  if cmd[0] = ReserverWord[RW_ROL]   then begin ROL_SSL_NSL(cmd, RW_ROL); goto gn; end;
  if cmd[0] = ReserverWord[RW_SSL]   then begin ROL_SSL_NSL(cmd, RW_SSL); goto gn; end;
  if cmd[0] = ReserverWord[RW_NSL]   then begin ROL_SSL_NSL(cmd, RW_NSL); goto gn; end;

  if cmd[0] = ReserverWord[RW_ROR]   then begin ROR_SSR_NSR(cmd, RW_ROR); goto gn; end;
  if cmd[0] = ReserverWord[RW_SSR]   then begin ROR_SSR_NSR(cmd, RW_SSR); goto gn; end;
  if cmd[0] = ReserverWord[RW_NSR]   then begin ROR_SSR_NSR(cmd, RW_NSR); goto gn; end;

  if cmd[0] = ReserverWord[RW_ROU]   then begin ROU_SSU_NSU(cmd, RW_ROU); goto gn; end;
  if cmd[0] = ReserverWord[RW_SSU]   then begin ROU_SSU_NSU(cmd, RW_SSU); goto gn; end;
  if cmd[0] = ReserverWord[RW_NSU]   then begin ROU_SSU_NSU(cmd, RW_NSU); goto gn; end;

  if cmd[0] = ReserverWord[RW_ROD]   then begin ROD_SSD_NSD(cmd, RW_ROD); goto gn; end;
  if cmd[0] = ReserverWord[RW_SSD]   then begin ROD_SSD_NSD(cmd, RW_SSD); goto gn; end;
  if cmd[0] = ReserverWord[RW_NSD]   then begin ROD_SSD_NSD(cmd, RW_NSD); goto gn; end;

  if cmd[0] = ReserverWord[RW_sleep] then begin SleepCMD(cmd);            goto gn; end;
  if cmd[0] = ReserverWord[RW_Wait]  then begin WaitCMD(cmd);             goto gn; end;
  if cmd[0] = ReserverWord[RW_skip]  then begin z := -1; Skip(cmd, z);    goto gn; end;
  if cmd[0] = '}skip'                then begin fSkipSeveral := false;    goto gn; end;

  if cmd[0] = ReserverWord[RW_NOT_ROW] then begin Or_Xor_Not_And_RowCol(cmd, RW_NOT_ROW); goto gn; end;
  if cmd[0] = ReserverWord[RW_NOT_COL] then begin Or_Xor_Not_And_RowCol(cmd, RW_NOT_COL); goto gn; end;
  if cmd[0] = ReserverWord[RW_OR_ROW]  then begin Or_Xor_Not_And_RowCol(cmd, RW_OR_ROW);  goto gn; end;
  if cmd[0] = ReserverWord[RW_OR_COL]  then begin Or_Xor_Not_And_RowCol(cmd, RW_OR_COL);  goto gn; end;
  if cmd[0] = ReserverWord[RW_XOR_ROW] then begin Or_Xor_Not_And_RowCol(cmd, RW_XOR_ROW); goto gn; end;
  if cmd[0] = ReserverWord[RW_XOR_COL] then begin Or_Xor_Not_And_RowCol(cmd, RW_XOR_COL); goto gn; end;
  if cmd[0] = ReserverWord[RW_AND_ROW] then begin Or_Xor_Not_And_RowCol(cmd, RW_AND_ROW); goto gn; end;
  if cmd[0] = ReserverWord[RW_AND_COL] then begin Or_Xor_Not_And_RowCol(cmd, RW_AND_COL); goto gn; end;

  if cmd[0] = ReserverWord[RW_save_led]  then begin SaveLed(cmd);  goto gn; end;
  if cmd[0] = ReserverWord[RW_load_led]  then begin LoadLed(cmd);  goto gn; end;
  if cmd[0] = ReserverWord[RW_clear_led] then begin ClearLed(cmd); goto gn; end;

  fSkip := 1;

  gn :
  FreeAndNil(cmd);
end;

procedure TParserScript.ParseSampleCode;
var
  i, c, p : Integer;
  str, s : String;
begin
  Status := AParserStatus;   //�� ��������� ����� ������ ����������(�������� �� ������ ����)
  FError := false;
  SampleCode.Clear; //������ ������� ����� ������� ���������� ����

  script.Clear;
  c := OriginalScript.Count-1;
  for i := 0 to c do
  begin
    p := Pos(';', OriginalScript[i]);
    if p = 0 then script.Add(OriginalScript[i]) else script.Add(Copy(OriginalScript[i], 1, p-1));
    script[i] := LowerCase(script[i].Trim);
  end;

  ParseMain;

  if FError then
  begin
    if SampleCode.Count > 0 then s := Format('������! ������ %d' + #10#13, [SampleCode[SampleCode.Count-1].Line]) else s := '';
    FErrorString := s + FErrorString;
    ListError.TryGetValue('CaptionCompileError', str);
    MessageBox(hwndForMsgBox, PChar(FErrorString), PChar(str), MB_OK+MB_ICONERROR);

    exit;
  end;
end;

procedure TParserScript.ROD_SSD_NSD(var cmd: TStringList; RW: Word);
var
  i, c, _col : integer;
  s_led : TLeds;
  cw : TList<Word>;
begin
  if Status = psCompiled then
  begin
    if not Column(cmd[1], cw) then begin cw.Free; exit; end;
    if cmd.Count = 3 then
      if not TryStrToInt(cmd[2], c) then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
  end
  else
  if Status = psRun then
    if Column(cmd[1], cw) then
    begin
    if fRepeat = 0 then
      if cmd.Count = 3 then
        if TryStrToInt(cmd[2], c) then
          if c > 1 then fRepeat := c;

      c := LedDisplay.Rows - 2;

      for _col in cw do //���������� �������� �� ����������
      begin
        case RW of
          RW_ROD : s_led := LedDisplay.Leds[LedDisplay.Rows-1, _col];
        end;

        for i := c downto 0 do LedDisplay.Leds[i+1, _col] := LedDisplay.Leds[i, _col]; //����� ����

        case RW of
          RW_ROD : LedDisplay.Leds[0, _col] := s_led;
          RW_SSD : LedDisplay.Leds[0, _col].Vis := true;
          RW_NSD : LedDisplay.Leds[0, _col].Vis := false;
        end;
      end;
    end;
  cw.Free
end;

procedure TParserScript.ROL_SSL_NSL(var cmd: TStringList; RW : Word);
var
  _line, i, c : integer;
  s_led : TLeds;
  lw : TList<Word>;
begin
  if Status = psCompiled then
  begin
    if  (cmd.Count > 3)or(cmd.Count < 2) then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
    if not line(cmd[1], lw) then begin lw.Free; exit; end;
    if cmd.Count = 3 then
      if not TryStrToInt(cmd[2], c) then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
  end
  else
  if Status = psRun then
    if line(cmd[1], lw) then
    begin
      if fRepeat = 0 then
      if cmd.Count = 3 then
        if TryStrToInt(cmd[2], c) then
          if c > 1 then fRepeat := c;


      c := LedDisplay.Cols - 2;

      for _line in lw do //���������� ����� �� ����������
      begin
        case RW of
          RW_ROL :  s_led := LedDisplay.Leds[_line, 0];
        end;

        for i := 0 to c do LedDisplay.Leds[_line, i] := LedDisplay.Leds[_line, i+1]; //����� � ����

        case RW of
          RW_ROL : LedDisplay.Leds[_line, LedDisplay.Cols-1] := s_led;
          RW_SSL : LedDisplay.Leds[_line, LedDisplay.Cols-1].Vis := true;
          RW_NSL : LedDisplay.Leds[_line, LedDisplay.Cols-1].Vis := false
        end;
      end;
    end;

  lw.Free;
end;

procedure TParserScript.ROR_SSR_NSR(var cmd: TStringList; RW : Word);
var
  i, c, _line : integer;
  s_led : TLeds;
  lw : TList<Word>;
begin
  if Status = psCompiled then
  begin
    if not line(cmd[1], lw) then begin lw.Free; exit; end;
    if cmd.Count = 3 then
      if not TryStrToInt(cmd[2], c) then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
  end
  else
  if Status = psRun then
    if line(cmd[1], lw) then
    begin
    if fRepeat = 0 then
      if cmd.Count = 3 then
        if TryStrToInt(cmd[2], c) then
          if c > 1 then fRepeat := c;

      c := LedDisplay.Cols - 2;

      for _line in lw do //���������� ����� �� ����������
      begin
        case RW of
          RW_ROR : s_led := LedDisplay.Leds[_line, LedDisplay.Cols-1];
        end;

        for i := c downto 0 do LedDisplay.Leds[_line, i+1] := LedDisplay.Leds[_line, i];

        case RW of
          RW_ROR : LedDisplay.Leds[_line, 0] := s_led;
          RW_SSR : LedDisplay.Leds[_line, 0].Vis := true;
          RW_NSR : LedDisplay.Leds[_line, 0].Vis := false;
        end;
      end;
    end;
  lw.Free;
end;

procedure TParserScript.ROU_SSU_NSU(var cmd: TStringList; RW: Word);
var
  i, c, _col : integer;
  s_led : TLeds;
  lw : TList<Word>;
begin
  if Status = psCompiled then
  begin
    if not Column(cmd[1], lw) then begin lw.Free; exit; end;
    if cmd.Count = 3 then
      if not TryStrToInt(cmd[2], c) then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
  end
  else
  if Status = psRun then
    if Column(cmd[1], lw) then
    begin
    if fRepeat = 0 then
      if cmd.Count = 3 then
        if TryStrToInt(cmd[2], c) then
          if c > 1 then fRepeat := c;

      c := LedDisplay.Rows - 2;

      for _col in lw do //���������� �������� �� ����������
      begin
        case RW of
          RW_ROU : s_led := LedDisplay.Leds[0, _col];
        end;

        for i := 0 to c do LedDisplay.Leds[i, _col] := LedDisplay.Leds[i+1, _col]; //����� �����

        case RW of
          RW_ROU : LedDisplay.Leds[LedDisplay.Rows-1, _col] := s_led;
          RW_SSU : LedDisplay.Leds[LedDisplay.Rows-1, _col].Vis := true;
          RW_NSU : LedDisplay.Leds[LedDisplay.Rows-1, _col].Vis := false;
        end;
      end;
    end;
  lw.Free;
end;

procedure TParserScript.SaveLed(var cmd: TStringList);
var
  lw, cw : TList<Word>;
  i, j, i2, j2 : Word;
  LedMem : TLedMemory;
  k : Word;
begin
//SaveLed row 2 <name param>
//SaveLed col 2 <name param>
//SaveLed all <name param>
  if Status = psCompiled then
  begin
    if (cmd.Count < 1)or(cmd.Count > 4) then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;

    if cmd[1] = 'row' then
    begin
      if not line(cmd[2], lw) then begin lw.Free; exit; end;
    end
    else if cmd[1] = 'col' then
    begin
      if not line(cmd[2], cw) then begin cw.Free; exit; end;
    end
    else if cmd[1] <> 'all' then begin FError := true; ListError.TryGetValue('Param', FErrorString); exit; end;
  end;

  if Status = psRun then
  begin
 //����� �������� �� ������������
    if (cmd[1] = 'all') then k := 2 else k := 3;
    if FindLedMemory(cmd[k], LedMem) then begin {ShowMessage('Led memory dublicate');} exit; end;

    if (cmd[1] = 'row')or(cmd[1] = 'all') then
    begin
      if (cmd[1] = 'row') then k := 2 else k := 1;
      if not line(cmd[k], lw) then begin lw.Free; exit; end;

      LedMem := TLedMemory.Create(lsmRow, lw, LedDisplay.Cols, cmd[k+1]);

      i2 := 0;
      for i in lw do
      begin
        j2 := 0;
        for j := 0 to LedDisplay.Cols-1 do
        begin
          LedMem.Leds[i2, j2].Vis := LedDisplay.Leds[i, j].Vis;
          LedMem.Leds[i2, j2].Color := LedDisplay.Leds[i, j].Color;
          Inc(j2);
        end;
        Inc(i2);
      end;
    end
    else if cmd[1] = 'col' then
    begin
      if not column(cmd[2], cw) then begin cw.Free; exit; end;

      LedMem := TLedMemory.Create(lsmCol, cw, LedDisplay.Rows, cmd[3]);

      i2 := 0;
      for i in cw do
      begin
        j2 := 0;
        for j := 0 to LedDisplay.Rows-1 do
        begin
          LedMem.Leds[j2, i2].Vis := LedDisplay.Leds[j, i].Vis;
          LedMem.Leds[j2, i2].Color := LedDisplay.Leds[j, i].Color;
          Inc(j2);
        end;
        Inc(i2);
      end;
    end;

    fLedMemory.Add(LedMem);
  end;
end;

procedure TParserScript.SetCol(var cmd: TStringList);
var
  col : Integer;
begin
  if Status = psCompiled then
  begin
    if cmd.Count = 1 then begin LedDisplay.Cols := LedDisplay.DefRows; end; //�����-------------------------------------
    if cmd.Count > 2 then begin FError := true; ListError.TryGetValue('LedColParam', FErrorString); exit; end;

    if not TryStrToInt(cmd[1], col) then begin FError := true; ListError.TryGetValue('LedColParam', FErrorString); exit; end;
    if (col > 0)and(col < 65535) then LedDisplay.Cols := col //�����----------------------------------------------------
      else begin FError := true; ListError.TryGetValue('LedColCountParam', FErrorString); exit; end;
  end;
end;

procedure TParserScript.SetRow(var cmd: TStringList);
var
  row : Integer;
begin
  if Status = psCompiled then
  begin
    if cmd.Count = 1 then begin LedDisplay.Rows := LedDisplay.DefRows; end; //�����  -----------------------------------
    if cmd.Count > 2 then begin FError := true; ListError.TryGetValue('LedRowParam', FErrorString); exit; end;

    if not TryStrToInt(cmd[1], row) then begin FError := true; ListError.TryGetValue('LedRowParam', FErrorString); exit; end;
    if (row > 0)and(row < 65535) then LedDisplay.Rows := row //����� ---------------------------------------------------
      else begin FError := true; ListError.TryGetValue('LedRowCountParam', FErrorString); exit; end;
  end;
end;

procedure TParserScript.SleepCMD(var cmd: TStringList);
var
  time : Integer;
begin
  if Status = psCompiled then
  begin
    if cmd.Count <> 2 then begin FError := true; ListError.TryGetValue('SleepParam', FErrorString); exit; end;

    if not TryStrToInt(cmd[1], time) then begin FError := true; ListError.TryGetValue('NumParam', FErrorString); exit; end;
    if time < 1 then begin FError := true; ListError.TryGetValue('ZeroParam', FErrorString); exit; end;
  end;

  if Status = psRun then
  begin
    Sleep := StrToInt(cmd[1]);
//    fSkip := 1;
  end;
end;

function TParserScript.TextToWord(const str : String; var sl : TStringList) :Boolean;
begin
  sl.Delimiter := ' ';
  sl.StrictDelimiter := true;
  sl.DelimitedText := str;
  Result := sl.Count >= 1;
end;

function TParserScript.TextToWord_LowerCase_Trim(const str : String; var sl : TStringList) :Boolean;
begin
  sl.Delimiter := ' ';
  sl.StrictDelimiter := true;
  sl.DelimitedText := LowerCase(str.Trim);
  Result := sl.Count >= 1;
end;

procedure TParserScript.WaitCMD(var cmd: TStringList);
var
  time : Integer;
begin
  if Status = psCompiled then
  begin
    if cmd.Count <> 2 then begin FError := true; ListError.TryGetValue('WaitParam', FErrorString); exit; end;

    if not TryStrToInt(cmd[1], time) then begin FError := true; ListError.TryGetValue('NumParam', FErrorString); exit; end;
    if time < 1 then begin FError := true; ListError.TryGetValue('ZeroParam', FErrorString); exit; end;
  end;

  if Status = psRun then Wait := StrToInt(cmd[1]);
end;

{ TLedMemory }

constructor TLedMemory.Create(AState : TLedStateMemory; AList :TList<Word>; CountRowCol : Word; const ANameParam : String);
var
  i, j : Word;
begin
  State := AState;
  list := AList;
  NameParam := ANameParam;


  if (AState = lsmRow)or(AState = lsmAll) then
  begin
    SetLength(Leds, AList.Count);

    for i in AList do
      SetLength(Leds[i], CountRowCol);
  end
  else if AState = lsmCol then
  begin
    SetLength(Leds, CountRowCol);

    for i := 0 to CountRowCol-1 do
      SetLength(Leds[i], AList.Count);
  end
  else if AState = lsmDot then ShowMessage('Not yet release!!!');


end;

destructor TLedMemory.Destroy;
begin
  if list <> nil then FreeAndNil(list);
  Leds := nil;
  inherited;
end;

end.
