unit uParseComCmd;

interface

uses System.Classes, System.SysUtils,  Vcl.Dialogs;

type
  TLedTowerSizeEvent = procedure(Sender: TObject; Row, Col : Integer) of object;

  TParseCmd = class(TObject)
  private
    Fcmd : byte;
    FBuf : TByteArray;
    FbufCur : Word;
    FLedTowerSizeEvent :TLedTowerSizeEvent;
    constructor Create; overload;
    procedure ParseLedSize(in_buf: PByteArray; Count : Integer);
    procedure ParseLedState(in_buf: PByteArray; Count : Integer);
  public
    procedure Parse(in_buf : PByteArray; Count : Integer);
    procedure PrepareToSendLedSize(var m : TByteArray; var Count : Word);
    procedure PrepareToSendSetHead(nHead : byte; var m : TByteArray; var Count : Word);
//    procedure SendLedState(PByteArr);
    property FOnLedTowerSizeEvent : TLedTowerSizeEvent read FLedTowerSizeEvent write FLedTowerSizeEvent;
  end;

const
  SendByte1 = $08;
  SendByte2 = $F5;

  RecvByte1 = $08;
  RecvByte2 = $F5;

  cmdComLedState = 5;
  cmdComLedStateBack = 6;
  cmdComLedSize = 20;
  cmdComSetHead = 30;

var
  cmd : TParseCmd;

implementation

{ TParseCmd }

constructor TParseCmd.Create;
begin
  Fcmd := 0;
  FbufCur := 0;
end;

procedure TParseCmd.Parse(in_buf: PByteArray; Count : Integer);
//var
//  s : String;
//  i : Word;
begin
//  s := '';
//  for i := 0 to Count-1 do s := s + ' ' + in_buf[i].ToString;
//  ShowMessage(s);

  if Fcmd = cmdComLedState then begin ParseLedState(in_buf, Count); exit; end;

  if Count < 3 then exit;


  if (in_buf[0] = RecvByte1)and(in_buf[1] = RecvByte2) then
  begin
    Fcmd := in_buf[2];
    case Fcmd of
      cmdComLedStateBack : ParseLedState(in_buf, Count);
      cmdComLedSize  : if Count = 5 then begin ParseLedSize(in_buf, Count); Fcmd := 0; end;
    end;
  end;
end;

procedure TParseCmd.ParseLedSize(in_buf: PByteArray; Count : Integer);
//const byte SizeLed[5] = {0x08, 0xf5, 20, LedRow, LedCol}; on arduino
begin
  if Assigned(FOnLedTowerSizeEvent) then FLedTowerSizeEvent(Self, in_buf[3], in_buf[4]);
//  ShowMessage(in_buf[3].ToString + ' ' + in_buf[4].ToString);
end;

procedure TParseCmd.ParseLedState(in_buf: PByteArray; Count: Integer);
var
  i : Word;
  s : String;
  label l1;
begin

  if Count = 35 then
  begin
    Fcmd := 0;
    Move(in_buf[0], FBuf[FbufCur], Count);
  end
  else
  begin
    Move(in_buf[0], FBuf[FbufCur], Count);
    Inc(FbufCur, Count);

    if FbufCur = 35 then
    begin
      FbufCur := 0;
      Fcmd := 0;
      goto l1;
    end;
    exit;
  end;

l1:
  s := '';
  for i := 3 to 34 do s := s + ' ' + Fbuf[i].ToString;
  ShowMessage(s);
end;

procedure TParseCmd.PrepareToSendLedSize(var m: TByteArray; var Count: Word);
begin
  m[0] := SendByte1;
  m[1] := SendByte2;
  m[2] := cmdComLedSize;
  Count := 3;
end;

procedure TParseCmd.PrepareToSendSetHead(nHead : byte; var m: TByteArray; var Count: Word);
begin
  m[0] := SendByte1;
  m[1] := SendByte2;
  m[2] := cmdComSetHead;
  m[3] := nHead;
  Count := 4;
end;

initialization
  cmd := TParseCmd.Create;

finalization
  cmd.Free;
  cmd := nil;

end.
